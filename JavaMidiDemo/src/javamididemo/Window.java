/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.sound.midi.MidiSystem;
import javax.sound.sampled.AudioSystem;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Main frame.
 * Displays Groove Builder, Song Builder and Key Synthesizer.
 * 
 * @author Andrej Hraško
 */
public class Window extends JPanel implements ChangeListener, Runnable {

    List panels = new ArrayList(4);
    JTabbedPane tabPane = new JTabbedPane();
    int index;
    int width = 640, height = 480;
//    int width = 1024, height = 700;
    KeySynthesizer keySynth;
//    SoundbankBuilder sBB;
    
    public Window() {
        setLayout(new BorderLayout());
        
        tabPane.addChangeListener(changeListener);
        
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
        BevelBorder bb = new BevelBorder(BevelBorder.LOWERED);
        CompoundBorder cb = new CompoundBorder(eb, bb);
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(new CompoundBorder(cb, new EmptyBorder(0, 0, 90, 0)));
        
        new Thread(this).start();
        
        add(tabPane, BorderLayout.CENTER);
    }
    
    public void run() {
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
        BevelBorder bb = new BevelBorder(BevelBorder.LOWERED);
        CompoundBorder cb = new CompoundBorder(eb, bb);
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(new CompoundBorder(cb, new EmptyBorder(0, 0, 90, 0)));
        
        GrooveBuilder grooveApp = new GrooveBuilder();
        panels.add(grooveApp);
        tabPane.addTab("Groove Builder", grooveApp);
        
        p = new JPanel(new BorderLayout());
        p.setBorder(new CompoundBorder(cb, new EmptyBorder(0, 0, 5, 20)));
        SongBuilder songBuilder = new SongBuilder();
        panels.add(songBuilder);
        p.add(songBuilder);
        tabPane.addTab("Song Builder", p);

        keySynth = new KeySynthesizer();
        panels.add(keySynth);
        tabPane.addTab("Key Synthesizer", keySynth);
        
//        sBB = new SoundbankBuilder();
//        panels.add(sBB);
//        tabPane.addTab("Soundbank Builder", sBB);
    }
    ChangeListener changeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
            int index = sourceTabbedPane.getSelectedIndex();
            if (sourceTabbedPane.getTitleAt(index).startsWith("Key")) {
                keySynth.table.grabFocus();
            }
        }
    };
    
    public void close() {
        ((ControlContext) panels.get(index)).close();
    }
    
    public void open() {
        ((ControlContext) panels.get(index)).open();
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }
    
    @Override
    public void stateChanged(ChangeEvent e) {
        close();
        System.gc();
        index = tabPane.getSelectedIndex();
        open();
    }
    
    public static void main(String[] args) {
        
        try {            
            if (MidiSystem.getSequencer() == null) {
                System.err.println("MidiSystem Sequencer Unavailable, exiting!");
                System.exit(1);
            } else if (AudioSystem.getMixer(null) == null) {
                System.err.println("AudioSystem Unavailable, exiting!");
                System.exit(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        final Window demo = new Window();
        JFrame f = new JFrame("Java MIDI demonstration app");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            public void windowDeiconified(WindowEvent e) {
                demo.open();
            }

            public void windowIconified(WindowEvent e) {
                demo.close();
            }
        });
        f.getContentPane().add("Center", demo);
        f.pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        f.setLocation(d.width / 2 - 1024 / 2, d.height / 2 - 750 / 2);
        f.setVisible(true);
    }
}
