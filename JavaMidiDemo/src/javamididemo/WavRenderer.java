/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import com.sun.media.sound.AudioSynthesizer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.*;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 *  Class for rendering WAVE sound file using receiver.
 * 
 * render() renders WAVE file, send() sends sequence to the sequencer using timestamps.
 * @author Andrej Hraško
 */
public class WavRenderer {


    public WavRenderer() {

    }

    /**
     * Renders WAV file using given data and soundbank.
     * Sequence is sent to the receiver using send() method.
     * @param soundbank Used soundbank
     * @param sequence 
     * @param output_file Destination file
     * @return true if succesfull
     * @throws MidiUnavailableException
     * @throws IOException 
     */
    public boolean render(Soundbank soundbank, Sequence sequence, File output_file) throws MidiUnavailableException, IOException {


        try (AudioSynthesizer synth = findAudioSynthesizer()) {
            if (synth == null) {
                System.out.println("No AudioSynhtesizer was found!");
                System.exit(1);
            }
            // Open AudioStream from AudioSynthesizer.
            AudioInputStream stream = synth.openStream(null, null);

            // Load user-selected Soundbank into AudioSynthesizer.
            if (soundbank != null) {
                Soundbank defsbk = synth.getDefaultSoundbank();
                if (defsbk != null) {
                    synth.unloadAllInstruments(defsbk);
                }
                synth.loadAllInstruments(soundbank);
            }

            // Play Sequence into AudioSynthesizer Receiver.
            double total = send(sequence, synth.getReceiver());

            // Calculate how long the WAVE file needs to be.
            long len = (long) (stream.getFormat().getFrameRate() * (total + 4));
            stream = new AudioInputStream(stream, stream.getFormat(), len);

            // Write WAVE file to disk.
            AudioSystem.write(stream, AudioFileFormat.Type.WAVE, output_file);
        }


        return true;
    }

    /**
     * Finds available AudioSynthesizer.
     */
    public static AudioSynthesizer findAudioSynthesizer() {
        // First check if default synthesizer is AudioSynthesizer.
        Synthesizer synth;
        try {
            synth = MidiSystem.getSynthesizer();

            if (synth instanceof AudioSynthesizer) {
                return (AudioSynthesizer) synth;
            }

            // If default synhtesizer is not AudioSynthesizer, check others.
            MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
            for (int i = 0; i < infos.length; i++) {
                MidiDevice dev = MidiSystem.getMidiDevice(infos[i]);
                if (dev instanceof AudioSynthesizer) {
                    return (AudioSynthesizer) dev;
                }
            }
        } catch (MidiUnavailableException ex) {
            Logger.getLogger(WavRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
        // No AudioSynthesizer was found, return null.
        return null;
    }

    /*
     * Send entry MIDI Sequence into Receiver using timestamps.
     */
    public static double send(Sequence seq, Receiver recv) {
        float divtype = seq.getDivisionType();
        assert (seq.getDivisionType() == Sequence.PPQ);
        Track[] tracks = seq.getTracks();
        int[] trackspos = new int[tracks.length];
        int mpq = 500000;
        int seqres = seq.getResolution();
        long lasttick = 0;
        long curtime = 0;
        while (true) {
            MidiEvent selevent = null;
            int seltrack = -1;
            for (int i = 0; i < tracks.length; i++) {
                int trackpos = trackspos[i];
                Track track = tracks[i];
                if (trackpos < track.size()) {
                    MidiEvent event = track.get(trackpos);
                    if (selevent == null
                            || event.getTick() < selevent.getTick()) {
                        selevent = event;
                        seltrack = i;
                    }
                }
            }
            if (seltrack == -1) {
                break;
            }
            trackspos[seltrack]++;
            long tick = selevent.getTick();
            if (divtype == Sequence.PPQ) {
                curtime += ((tick - lasttick) * mpq) / seqres;
            } else {
                curtime = (long) ((tick * 1000000.0 * divtype) / seqres);
            }
            lasttick = tick;
            MidiMessage msg = selevent.getMessage();
            if (msg instanceof MetaMessage) {
                if (divtype == Sequence.PPQ) {
                    if (((MetaMessage) msg).getType() == 0x51) {
                        byte[] data = ((MetaMessage) msg).getData();
                        mpq = ((data[0] & 0xff) << 16)
                                | ((data[1] & 0xff) << 8) | (data[2] & 0xff);
                    }
                }
            } else {
                if (recv != null) {
                    recv.send(msg, curtime);
                }
            }
        }
        return curtime / 1000000.0;
    }
    
    
}
