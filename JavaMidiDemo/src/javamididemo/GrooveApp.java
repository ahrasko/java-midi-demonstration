/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import javax.sound.midi.*;
import javax.swing.*;
import java.util.logging.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.List;

/**
 * App for building tracks using instruments from default sounbank.
 *
 * Created tracks can be saved as .DB files and used in the SongBuilder App or
 * exported into MIDI or WAVE.
 *
 * @author Andrej Hraško
 */
public class GrooveApp extends JPanel implements ActionListener, ControlContext, MetaEventListener {

    final int PROGRAM_CHANGE = 192;
    final int NOTE_ON = 144;
    final int NOTE_OFF = 128;
    boolean maxTickNum = true;
    DatabaseHandler handler;
    Sequencer sequencer;
    Sequence sequence;
    Synthesizer synthesizer;
    WavRenderer wavRenderer;
    Track track;
    TableModel dataModel;
    JTable table;
    JMenuItem ticks;
    JButton startButton, addButton, editButton, deleteButton, loopButton;
    JComboBox<Instrument> instrumentComboBox;
    JTextField noteField, velocityField, instrumentField, trackName;
    JTextPane statusPane;
    JScrollBar velocityScroll, bpmScroll;
    int row, column;
    Instrument instruments[];
    List<Data> data;

    public GrooveApp() {
        try {
            if (synthesizer == null) {
                synthesizer = MidiSystem.getSynthesizer();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        open();

        wavRenderer = new WavRenderer();
        handler = new DatabaseHandler();

        instruments = synthesizer.getDefaultSoundbank().getInstruments();
        data = new ArrayList(instruments.length);

        setLayout(new BorderLayout(5, 0));
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);

        setBorder(eb);
        //inserting first 10 instruments
        for (int i = 0; i < 10; i++) {
            data.add(new Data(i, instruments[i].getName() + " :" + "50", i, 0, 50, 100));
        }
        final String[] names = {"Instrument",
            "1", "2", "3", "4",
            "5", "6", "7", "8",
            "9", "10", "11", "12",
            "13", "14", "15", "16",
            "17", "18", "19", "20"};
        dataModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return names.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    return ((Data) data.get(row)).name;
                } else {
                    return ((Data) data.get(row)).staff[col - 1];
                }
            }

            @Override
            public String getColumnName(int col) {
                return names[col];
            }

            @Override
            public Class getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int row, int col) {
                if (col == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public void setValueAt(Object value, int row, int col) {
                if (col == 0) {
                    ((Data) data.get(row)).name = (String) value;
                } else {
                    ((Data) data.get(row)).staff[col - 1] = (Color) value;
                }
            }
        };
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            @Override
            public void setValue(Object value) {
                setBackground((Color) value);
            }
        };
        //creating new table with defined settings
        table = new JTable(dataModel);

        table.getColumn(names[0]).setMinWidth(120);
        TableColumnModel tableCM = table.getColumnModel();
        for (int i = 1;
                i < names.length;
                i++) {
            TableColumn col = tableCM.getColumn(i);
            col.setCellRenderer(renderer);
        }
        // Listener for row changes
        ListSelectionModel lsm = table.getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            row = sm.getMinSelectionIndex();
                            loadInstrumentData();
                        }
                    }
                });
        // Listener for column changes
        lsm = table.getColumnModel().getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            column = sm.getMinSelectionIndex();
                        }
                        if (column != 0) {
                            Color c = ((Data) data.get(row)).staff[column - 1];
                            if (c.equals(Color.white)) {
                                ((Data) data.get(row)).staff[column - 1] = Color.black;
                            } else {
                                ((Data) data.get(row)).staff[column - 1] = Color.white;
                            }
                            table.tableChanged(new TableModelEvent(dataModel));
                        }
                    }
                });

        JPanel mainPanel = new JPanel();

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        SoftBevelBorder sbb = new SoftBevelBorder(BevelBorder.RAISED);
        mainPanel.add(Box.createVerticalStrut(10));

        //menu bar
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menu.getAccessibleContext().setAccessibleDescription("file menu");
        menuBar.add(menu);

        JMenu edit = new JMenu("Edit");
        edit.setMnemonic(KeyEvent.VK_E);
        edit.getAccessibleContext().setAccessibleDescription("edit menu");
        menuBar.add(edit);

        JMenu infoMenu = new JMenu("Info");
        infoMenu.setMnemonic(KeyEvent.VK_I);
        menuBar.add(infoMenu);

        JMenuItem about = new JMenuItem("About");
        about.setToolTipText("Info about the app.");
        about.addActionListener(this);
        infoMenu.add(about);

        //a group of JMenuItems
        JMenuItem menuSave = new JMenuItem("Save File", KeyEvent.VK_S);
        menuSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        menu.add(menuSave);
        menuSave.addActionListener(this);

        JMenuItem menuLoad = new JMenuItem("Load File", KeyEvent.VK_L);
        menuLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
        menu.add(menuLoad);
        menuLoad.addActionListener(this);

        JMenuItem exportMIDI = new JMenuItem("Export MIDI", KeyEvent.VK_M);
        exportMIDI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
        menu.add(exportMIDI);
        exportMIDI.addActionListener(this);
        JMenuItem exportWAV = new JMenuItem("Export WAV", KeyEvent.VK_W);
        exportWAV.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
        menu.add(exportWAV);
        exportWAV.addActionListener(this);

        ticks = new JCheckBoxMenuItem("Fixed ticks");
        ticks.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
        ticks.setSelected(maxTickNum);
        edit.add(ticks);
        ticks.setActionCommand("Fix 20 ticks.");
        ticks.addActionListener(this);

        JMenuItem menuExit = new JMenuItem("Close", KeyEvent.VK_L);
        menuExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        menuExit.getAccessibleContext().setAccessibleDescription("This will close the app.");
        menu.add(menuExit);

        //includes button and instrument panel
        JPanel secondPanel = new JPanel();
        secondPanel.setLayout(new BoxLayout(secondPanel, BoxLayout.X_AXIS));

        //buttons
        JPanel buttonPanel = new JPanel(new GridLayout(4, 3));
        buttonPanel.setBorder(new CompoundBorder(sbb, eb));
        buttonPanel.add(startButton = createButton("Start", getBackground()));
        buttonPanel.add(trackName = new JTextField("default_name"));
        buttonPanel.add(loopButton = createButton("Loop", getBackground()));
        buttonPanel.add(createButton("Clear Grid", getBackground()));
        buttonPanel.add(createButton("Clear Instruments", getBackground()));
        buttonPanel.add(addButton = createButton("Add Instrument", getBackground()));
        buttonPanel.add(editButton = createButton("Edit Instrument", getBackground()));
        buttonPanel.add(deleteButton = createButton("Delete Instrument", getBackground()));

        //instrument panel
        JPanel instrumentPanel = new JPanel(new GridLayout(4, 2));
        instrumentPanel.setBorder(new CompoundBorder(sbb, eb));
        instrumentPanel.add(new JLabel("Instrument"));
        instrumentComboBox = createComboBox();
        instrumentComboBox.setRenderer(new InstrumentCellRenderer());
        instrumentPanel.add(instrumentComboBox);

        instrumentPanel.add(new JLabel("Note [0..127]"));
        instrumentPanel.add(noteField = new JTextField());
        instrumentPanel.add(new JLabel("Velocity"));
        instrumentPanel.add(velocityScroll = new JScrollBar(JScrollBar.HORIZONTAL, 100, 2, 0, 127));
        instrumentPanel.add(new JLabel("BPM"));
        instrumentPanel.add(bpmScroll = new JScrollBar(JScrollBar.HORIZONTAL, 120, 2, 40, 160));

        secondPanel.add(buttonPanel);
        secondPanel.add(instrumentPanel);


        statusPane = new JTextPane();
        statusPane.setEditable(false);

        add("North", menuBar);
        mainPanel.add(secondPanel);
        mainPanel.add(Box.createVerticalStrut(10));

        add("South", mainPanel);
        mainPanel.add(statusPane);

        add("Center", new JScrollPane(table));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object object = e.getSource();

        if (object instanceof JButton) {
            JButton b = (JButton) object;
            if (b.equals(startButton)) {
                if (b.getText().equals("Start")) {
                    startSequencer();
                    statusPane.setText("STATUS: Playing..");
                    b.setText("Stop");
                } else {
                    sequencer.stop();
                    b.setText("Start");
                }
            } else if (b.getText().equals("Clear Grid")) {
                clearTable();
                table.tableChanged(new TableModelEvent(dataModel));
            } else if (b.getText().equals("Clear Instruments")) {
                data.clear();
                table.tableChanged(new TableModelEvent(dataModel));
            } else if (b.getText().equals("Add Instrument")) {
                addInstrument();
            } else if (b.getText().equals("Delete Instrument")) {
                deleteInstrument();
            } else if (b.getText().equals("Edit Instrument")) {
                editInstrument();
            } else if (b.equals(loopButton)) {
                b.setSelected(!b.isSelected());
                if (loopButton.getBackground().equals(Color.gray)) {
                    loopButton.setBackground(getBackground());
                } else {
                    loopButton.setBackground(Color.gray);
                }
            }
        } else if (object instanceof JMenuItem) {
            JMenuItem m = (JMenuItem) object;
            if (m.getText().startsWith("Save")) {
                if (isAlphaNumeric(trackName.getText())) {
                    handler.initTables(trackName.getText());
                    for (int i = 0; i < data.size(); i++) {
                        handler.save((Data) data.get(i), trackName.getText());
                    }
                } else {
                    statusPane.setText("ERROR: Wrong name format. Use only alphanumeric characters.");
                }
            } else if (m.getText().startsWith("Load")) {
                File file = new File("trackDir/");

                final JFileChooser fc = new JFileChooser(file);
                fc.setFileFilter(new javax.swing.filechooser.FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        }
                        String name = f.getName();
                        if (name.endsWith(".db")) {
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public String getDescription() {
                        return ".db";
                    }
                });
                int returnVal = fc.showOpenDialog(GrooveApp.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    file = fc.getSelectedFile();
                    String c = file.getName();
                    int pos = c.lastIndexOf(".");
                    if (pos > 0) {
                        c = c.substring(0, pos);
                    }
                    data = handler.load(c);
                    trackName.setText(c);
                    table.tableChanged(new TableModelEvent(dataModel));
                    statusPane.setText("STATUS: " + c + " loaded.");
                    System.out.println("Loading: " + file.getName());
                } else {
                    System.out.println("Open command cancelled by user.");
                }

            } else if (m.equals(ticks)) {
                if (maxTickNum) {
                    maxTickNum = false;
                    statusPane.setText("STATUS: Ticks depend on track");
                } else {
                    maxTickNum = true;
                    statusPane.setText("STATUS: Fixed to all 20 ticks.");
                }
            } else if (m.getText().startsWith("Export MIDI")) {
                renderMIDI(trackName.getText());
                statusPane.setText("STATUS: MIDI track saved.");
            } else if (m.getText().startsWith("Close")) {
                System.exit(0);
            } else if (m.getText().startsWith("Export WAV")) {
                buildTrack();
                try {
                    wavRenderer.render(synthesizer.getDefaultSoundbank(), sequence, new File(trackName.getText() + ".wav"));
                } catch (MidiUnavailableException | IOException ex) {
                    statusPane.setText("ERROR: " + ex);
                    Logger.getLogger(GrooveApp.class.getName()).log(Level.SEVERE, null, ex);
                }
                statusPane.setText("STATUS: WAVE track saved.");
            } else if (m.getText().startsWith("About")) {
                showAboutDialog();
            }
        }
    }

    /**
     * Inserts instrument into the table based on data gathered from
     * coresponding components.
     */
    private void addInstrument() {
        try {
            Data d = (Data) instrumentComboBox.getSelectedItem();
            int id = d.instrId;
            int note = Integer.parseInt(noteField.getText());
            int velocity = velocityScroll.getValue();
            data.add(new Data(id, instruments[id].getName() + " :" + note, data.size(), 0, note, velocity));
            table.tableChanged(new TableModelEvent(dataModel));
            statusPane.setText("STATUS: Instrument " + instruments[id].getName() + " added. Note: " + note + ", velocity: " + velocity + ".");
        } catch (NumberFormatException ex) {
            statusPane.setText("ERROR: Invalid input format. Use Integer [0..127].");
        } catch (ArrayIndexOutOfBoundsException ex) {
            statusPane.setText("ERROR: Invalid input. Use Integer [0..127].");
        }
    }

    /**
     * Removes instrument in the selected row.
     *
     */
    private void deleteInstrument() {
        try {
            data.remove(data.get(row));
            table.clearSelection();
            table.tableChanged(new TableModelEvent(dataModel));
            noteField.setText("");
            statusPane.setText("STATUS: Instrument " + ((Data) data.get(row)).name + " removed.");
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
            System.out.println(ex);
        }
    }

    /**
     * Loads new data into displayed components.
     */
    private void loadInstrumentData() {
        Data temp = (Data) data.get(row);
        instrumentComboBox.setSelectedIndex(temp.instrId);
        noteField.setText(Integer.toString(temp.note));
        velocityScroll.setValue(temp.velocity);
    }

    /**
     * Updates selected instrument with new data. New data are obtained from
     * Components of the application.
     */
    private void editInstrument() {
        try {
            Data temp = (Data) instrumentComboBox.getSelectedItem();
            Data result = (Data) data.get(row);
            result.instrId = temp.instrId;
            result.note = Integer.parseInt(noteField.getText());
            result.name = temp.name + " :" + result.note;
            result.velocity = velocityScroll.getValue();
            data.set(row, result);
            table.tableChanged(new TableModelEvent(dataModel));
            statusPane.setText("STATUS: Instrument updated.");
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
            System.out.println(ex);
        }
    }

    /**
     * Decides whether the string is constructed only of aplhanumeric
     * characters.
     *
     * @param s String parameter.
     * @return true if the string is strictly alphanumeric, false otherwise.
     */
    public boolean isAlphaNumeric(String s) {
        String pattern = "^[a-zA-Z0-9_]*$";
        if (s.matches(pattern)) {
            return true;
        }
        return false;
    }

    /**
     * Renders and exports MIDI file from constructed sequence;
     *
     * @param filename Name of the generated MIDI file.
     */
    private void renderMIDI(String filename) {
        buildTrack();

        int[] allowedTypes = MidiSystem.getMidiFileTypes(sequence);
        if (allowedTypes.length == 0) {
            System.err.println("No supported MIDI file types.");
            statusPane.setText("ERROR: No supported MIDI file types.");
        } else {
            try {
                MidiSystem.write(sequence, allowedTypes[0], new File(filename + ".midi"));
            } catch (IOException ex) {
                statusPane.setText("ERROR: " + ex);
            }
        }
    }

    /**
     * Builds track based on informations from the table and loaded tracks.
     * Rekursively runs trough all staff records of all {@link Data} objects in
     * all loaded tracks.
     */
    private void buildTrack() {
        try {
            sequence = new Sequence(Sequence.PPQ, 4);
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
        track = sequence.createTrack();
        for (int i = 0; i < data.size(); i++) {
            Data row = (Data) data.get(i);
            for (int j = 0; j < row.staff.length; j++) {
                if (row.staff[j].equals(Color.BLACK)) {
                    createEvent(PROGRAM_CHANGE, 1, row.instrId, row.velocity, j);
                    createEvent(NOTE_ON, 1, row.note, row.velocity, j);
                    createEvent(NOTE_OFF, 1, row.note, row.velocity, j + 1);
                }
            }
        }
//      if the max ticks option is true
        if (maxTickNum) {
            createEvent(PROGRAM_CHANGE, 1, 1, 0, 20);
        }
    }

    /**
     * Starts the sequencer after building complete track.
     */
    private void startSequencer() {
        buildTrack();
        // set and start the sequencer.
        try {
            sequencer.setSequence(sequence);
        } catch (InvalidMidiDataException ex) {
            statusPane.setText("ERROR: " + ex);
        }
        sequencer.start();
        sequencer.setTempoInBPM(bpmScroll.getValue());
    }

    /**
     * Creates one MIDI Event based on given atributes. Type of the event is
     * determined by the [type] atribute. PROGRAM_CHANGE will change loaded
     * instrument. NOTE_ON will play one note. NOTE_OFF will stop currently
     * played note.
     *
     * @param type Type of the event.
     * @param chan Channel for which is event created.
     * @param num Instrument id for PROGRAM_CHANGE, note for NOTE_ON/OFF.
     * @param velocity Velocity of the event.
     * @param tick Tick when will the event occured.
     */
    private void createEvent(int type, int chan, int num, int velocity, long tick) {
        ShortMessage message = new ShortMessage();
        try {
            message.setMessage(type, chan, num, velocity);
            MidiEvent event = new MidiEvent(message, tick);
            track.add(event);
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
    }

    @Override
    public void meta(MetaMessage message) {
        if (message.getType() == 47) {  // 47 is end of track

            if (loopButton.getBackground().equals(Color.gray)) {
                if (sequencer != null && sequencer.isOpen()) {
                    startSequencer();
                    sequencer.setTempoInBPM(bpmScroll.getValue());
                }
            } else {
                startButton.setText("Start");
            }
            statusPane.setText("STATUS: Stopped.");
        }
    }

    @Override
    public final void open() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
        sequencer.addMetaEventListener(this);
    }

    @Override
    public void close() {
        if (startButton.getText().equals("Stop")) {
            startButton.doClick(0);
        }
        if (sequencer != null) {
            sequencer.close();
        }
        sequencer = null;
    }

    /**
     * Repaints every cell in the grid to WHITE.
     */
    private void clearTable() {
        for (int i = 0; i < data.size(); i++) {
            Data d = (Data) data.get(i);
            for (int j = 0; j < d.staff.length; j++) {
                d.staff[j] = Color.white;
            }
        }
    }

    /**
     * Creates JButton using given atributes.
     *
     * @param name Button label.
     * @param c Button color.
     * @return JButton object.
     */
    private JButton createButton(String bName, Color c) {
        JButton b = new JButton(bName);
        b.setBackground(c);
        b.addActionListener(this);
        return b;
    }

    /**
     * Creates JComboBox.
     *
     * @return JComboBox object.
     */
    private JComboBox<Instrument> createComboBox() {
        JComboBox combo;
        ArrayList list = new ArrayList(instruments.length);
        int l = instruments.length > 128 ? 128 : instruments.length;
        for (int i = 0; i < l; i++) {
            list.add(new Data(i, instruments[i].getName(), i, 0, 50, 100));
        }
        combo = new JComboBox(list.toArray());
        combo.addActionListener(this);
        return combo;
    }

    /**
     * Custom cell renderer for used combobox.
     */
    class InstrumentCellRenderer implements ListCellRenderer {

        protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
        private final Dimension preferredSize = new Dimension(0, 20);

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {
            JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                    isSelected, cellHasFocus);
            if (value instanceof Instrument) {
                renderer.setText(value.toString());
            }
            renderer.setPreferredSize(preferredSize);
            return renderer;
        }
    }

    /**
     * Dialog in the [About] Frame.
     */
    private static void showAboutDialog() {
        final String msg =
                "This App serves for creating beats by checking cells int the main grid.\n\n"
                + "Building the beat:\n"
                + "Just click the cells in the grid and press [Start]. [Loop] button will\n"
                + "enable continuous playing. You can still edit the grid during the loop.\n"
                + "If you are not satisfied with your first try, feel free to [Clear] the grid\n"
                + "and build new, better beat. Uncheck [Fixed ticks] if you want to build track\n"
                + "shorter than 20 ticks. \n\n"
                + "Instruments:\n"
                + "You can [Add] new Instruments into the grid by choosing one from the drop down\n"
                + "list and setting the note and velocity of it. You can also [Edit] existing\n"
                + "Instrument by selecting it in the grid and changing its settings.\n"
                + "[Clear Instruments] will Delete every Instrument in the grid.\n\n"
                + "Save & Export: \n"
                + "[Save] your track into .sql file for later work or [Export] it as a MIDI file.\n"
                + "To [Load] just select the .db file with the name of the track, the rest will be done.";
        new Thread(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(null, msg, "Applet Info", JOptionPane.INFORMATION_MESSAGE);
            }
        }).start();
    }

    public static void main(String args[]) {
        final GrooveApp groove = new GrooveApp();
        JFrame f = new JFrame("Track App");

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.getContentPane().add("Center", groove);
        f.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int w = 640;
        int h = 480;
        f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
        f.setSize(w, h);
        f.show();
        groove.open();

    }
}
