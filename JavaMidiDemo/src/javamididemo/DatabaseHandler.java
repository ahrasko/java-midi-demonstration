/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.awt.Color;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import javamididemo.Data;

/**
 * Class for loading and saving Data objects into .db files 
 * 
 * @author Andrej Hraško
 */
public class DatabaseHandler {

    public DatabaseHandler() {
        File file = new File("trackDir/");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    /**
     * Loads records of Data objects from given file and returns them as an ArrayList.
     * @param fileName Name of the DB file.
     * @return ArrayList of Data objects.
     */
    public ArrayList load(String fileName) {
        ArrayList newData = new ArrayList();
        Data data;
        Color staff[];

        Connection trackConn = null;
        Connection staffsConn = null;
        PreparedStatement trackStat = null;
        PreparedStatement staffsStat = null;

        try {
            Class.forName("org.sqlite.JDBC");
            trackConn = DriverManager.getConnection("jdbc:sqlite:trackDir/" + fileName + ".db");
            staffsConn = DriverManager.getConnection("jdbc:sqlite:trackDir/" + fileName + "staffs.db");

            trackStat = trackConn.prepareStatement("SELECT * FROM " + fileName + ";");

            boolean result = trackStat.execute();
            if (result) {
                ResultSet rs = trackStat.getResultSet();
                while (rs.next()) {
                    int instrId = rs.getInt(1);
                    String name = rs.getString(2);
                    int id = rs.getInt(3);
                    int lastTick = rs.getInt(4);
                    int note = rs.getInt(5);
                    int vel = rs.getInt(6);
                    data = new Data(instrId, name, id, lastTick, note, vel);
                    staff = new Color[20];
                    staffsStat = staffsConn.prepareStatement("SELECT * FROM " + fileName + "staffs WHERE id=?");
                    staffsStat.setInt(1, id);
                    boolean result2 = staffsStat.execute();
                    if (result2) {
                        ResultSet rs2 = staffsStat.getResultSet();
                        while (rs2.next()) {
                            for (int i = 2; i < 22; i++) {
                                if (rs2.getInt(i) == 1) {
                                    staff[i - 2] = Color.BLACK;
                                } else {
                                    staff[i - 2] = Color.WHITE;
                                }
                            }
                        }
                    }
                    data.setStaff(staff);
                    newData.add(data);
                }
            }

        } catch (SQLException ex) {
            System.out.println("load " + ex);
        } finally {
            closeStatement(trackStat);
            closeConnection(trackConn);
            closeStatement(staffsStat);
            closeConnection(staffsConn);

            return newData;
        }


    }

    /**
     * Saves state of the data object to the .db files.
     *
     * @param data
     */
    public void save(Data data, String name) {

        Connection trackConn = null;
        Connection staffsConn = null;
        PreparedStatement trackStat = null;
        PreparedStatement staffsStat = null;
        
        int lastTick = 0;
        
        try {
            trackConn = DriverManager.getConnection("jdbc:sqlite:trackDir/" + name + ".db");
            staffsConn = DriverManager.getConnection("jdbc:sqlite:trackDir/" + name + "staffs.db");

            Color[] c = data.getStaff();
            staffsStat = staffsConn.prepareStatement("INSERT INTO " + name + "staffs (id,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            staffsStat.setInt(1, data.getId());

            for (int i = 0; i < 20; i++) {
                if (c[i].equals(Color.BLACK)) {
                    staffsStat.setInt(i + 2, 1);
                    lastTick = i+1;
                } else {
                    staffsStat.setInt(i + 2, 0);
                }
            }
            staffsStat.addBatch();

            staffsConn.setAutoCommit(false);
            staffsStat.executeBatch();
            staffsConn.setAutoCommit(true);
            
            trackStat = trackConn.prepareStatement("INSERT INTO " + name + " (instrID, name, id, lastTick, note, velocity) VALUES (?, ?, ?, ?, ?, ?)");
            trackStat.setInt(1, data.getInstrId());
            trackStat.setString(2, data.getName());
            trackStat.setInt(3, data.getId());
            trackStat.setInt(4, lastTick);
            trackStat.setInt(5, data.getNote());
            trackStat.setInt(6, data.getVelocity());
            trackStat.addBatch();

            trackConn.setAutoCommit(false);
            trackStat.executeBatch();
            trackConn.setAutoCommit(true);


        } catch (SQLException ex) {
            //logger.error("Insert User SQL Exception", ex);
            System.out.println("save " + ex);
        } finally {
            closeStatement(trackStat);
            closeConnection(trackConn);
            closeStatement(staffsStat);
            closeConnection(staffsConn);
        }
    }

    /**
     * Initialize tables for tracks and staffs.
     * @param name 
     */
    public void initTables(String name) {
        Connection trackConn;
        Connection staffsConn;

        try {
            Class.forName("org.sqlite.JDBC");
            trackConn = DriverManager.getConnection("jdbc:sqlite:trackDir/" + name + ".db");
            staffsConn = DriverManager.getConnection("jdbc:sqlite:trackDir/" + name + "staffs.db");
            Statement trackStat = trackConn.createStatement();
            Statement staffsStat = staffsConn.createStatement();
            trackStat.executeUpdate("drop table if exists " + name + ";");
            staffsStat.executeUpdate("drop table if exists " + name + "staffs;");
            trackStat.executeUpdate("create table " + name + " ( instrId, name, id, lastTick, note, velocity);");
            staffsStat.executeUpdate("create table " + name + "staffs ( id,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20);");
        } catch (SQLException ex) {
            System.out.println("init " + ex);
            //logger.error("Init tables sql exception ", ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("init" + ex);

            //logger.error("Init tables ClassNotFoundException", ex);
        } finally {
        }
    }

    /**
     * Statement closing.
     *
     * @param statement Statement to be closed.
     */
    public static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                //logger.error("Close Statement SQL Exception", ex);
            }
        }
    }

    /**
     * Connection closing,
     *
     * @param connection Connection to be closed.
     */
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                //logger.error("Close Connection SQL Exception", ex);
            }
        }
    }

    /**
     * Method used for assign ID to new track. Used in insertFunction(Function)
     *
     * @param keys Generated key from preparedStatement
     * @return key for the track.
     * @throws SQLException
     */
    public int getId(ResultSet keys) throws SQLException {

        if (keys.getMetaData().getColumnCount() != 1) {
            //logError("Resultset in getId() had more columns.");
            throw new IllegalArgumentException("resultset has more columns");
        }

        if (keys.next()) {
            int Id = keys.getInt(1);
            if (keys.next()) {
                //logError("Resultset in getId() had more than one row.");
                throw new IllegalArgumentException("resultset has more rows");
            } else {
                return Id;
            }

        } else {
            //logError("Resultset in getId() had no rows.");
            throw new IllegalArgumentException("resultset has no rows");

        }
    }
}
