/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import javax.sound.midi.*;
import javax.swing.*;
import java.util.Vector;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.event.KeyEvent;

/**
 *
 * @author Andrej Hraško
 */
public class SynthTest extends JPanel implements ActionListener, ControlContext, MetaEventListener, KeyListener {

    Sequencer sequencer;
    Synthesizer synthesizer;
    Sequence sequence;
    Line track;
    ChannelData channels[];
    ChannelData cc;    // current channel
    JTable table;
    TableModel dataModel;
    Instrument instruments[];
    Vector data;
    int row, column;
    long startTime;

    public SynthTest() {
        try {
            if (synthesizer == null) {
                synthesizer = MidiSystem.getSynthesizer();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        addKeyListener(this);
        instruments = synthesizer.getDefaultSoundbank().getInstruments();
        data = new Vector(instruments.length);

        setLayout(new BorderLayout(5, 0));
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
        setBorder(eb);

        //inserting instruments into row[i] objects
        for (int i = 0; i < 4; i++) {
            data.add(new Data(i, instruments[i].getName() + " :" + "50", i, 0, 50, 100));
        }

        final String[] names = {"Instrument", "1", "2", "3", "4",
            "5", "6", "7", "8", "9", "10"};

        dataModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return names.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    return ((Data) data.get(row)).name;
                } else {
                    return ((Data) data.get(row)).staff[col - 1];
//                    return 1;
                }
            }

            @Override
            public String getColumnName(int col) {
                return names[col];
            }

            @Override
            public Class getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int row, int col) {
//                if (col == 0) {
                return false;
//                } else {
//                    return true;
//                }
            }

            @Override
            public void setValueAt(Object value, int row, int col) {
                if (col == 0) {
                    ((Data) data.get(row)).name = (String) value;
                } else {
                    ((Data) data.get(row)).staff[col - 1] = (Color) value;
//                    ((Data) data.get(row)).instrId = (Integer) value;                    
                }
            }
        };
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            @Override
            public void setValue(Object value) {
                setBackground((Color) value);
            }
        };

        //creating new table with defined settings
        table = new JTable(dataModel);

        table.getColumn(names[0]).setMinWidth(120);
        TableColumnModel tableCM = table.getColumnModel();
        for (int i = 1;
                i < names.length;
                i++) {
            TableColumn col = tableCM.getColumn(i);
            col.setCellRenderer(renderer);
        }

        table.setRowHeight(45);
        table.setTableHeader(null);
        // Listener for row changes
        ListSelectionModel lsm = table.getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            row = sm.getMinSelectionIndex();
//                            loadInstrumentData();
                        }
                    }
                });
        // Listener for column changes
        lsm = table.getColumnModel().getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            column = sm.getMinSelectionIndex();
                        }
                        if (column != 0) {
                            Color c = ((Data) data.get(row)).staff[column - 1];
                            if (c.equals(Color.white)) {
                                ((Data) data.get(row)).staff[column - 1] = Color.black;
                            } else {
                                ((Data) data.get(row)).staff[column - 1] = Color.white;
                            }
                            table.tableChanged(new TableModelEvent(dataModel));
                        }
                    }
                });

        JPanel mainPanel = new JPanel();

        mainPanel.setLayout(
                new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        SoftBevelBorder sbb = new SoftBevelBorder(BevelBorder.RAISED);
        mainPanel.add(Box.createVerticalStrut(10));
        
        //menu bar
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menu.getAccessibleContext().setAccessibleDescription("file menu");
        menuBar.add(menu);

        JMenuItem menuExit = new JMenuItem("Close", KeyEvent.VK_L);
        menuExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        menuExit.getAccessibleContext().setAccessibleDescription("This will close the app.");
        menu.add(menuExit);

        JTextField textField = new JTextField();
        textField.addKeyListener(this);
        
        add("South", textField);
        add("North", menuBar);
        add("Center", new JScrollPane(table));
    }

    private void createEvent(int type, int chan, int num, int velocity) {
    }

    private void programChange(int instrument) {
        if (instruments != null) {
            synthesizer.loadInstrument(instruments[instrument]);
        }
        cc.channel.programChange(instrument);
//            if (record) {
//                createShortEvent(PROGRAM, instrument);
//            }
    }

    public void on(int instrument, int note) {
//            setNoteState(ON);
        programChange(instrument);
        cc.channel.noteOn(note, cc.velocity);
//            if (record) {
//                createShortEvent(NOTEON, kNum);
//            }
    }

    public void off(int note) {
//            setNoteState(OFF);
        cc.channel.noteOff(note, cc.velocity);
//            if (record) {
//                createShortEvent(NOTEOFF, kNum);
//            }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent evt) {

        switch (evt.getKeyCode()) {
            case KeyEvent.VK_Q:
                System.out.println("Q pressed");
                on(0, 30);
                break;
            case KeyEvent.VK_W:
                on(0, 35);
                break;
            case KeyEvent.VK_E:
                break;
            case KeyEvent.VK_R:
                break;
            case KeyEvent.VK_T:
                break;
            case KeyEvent.VK_Z:
                break;
            case KeyEvent.VK_U:
                break;
            case KeyEvent.VK_I:
                break;
            case KeyEvent.VK_O:
                break;
            case KeyEvent.VK_P:
                break;
            default:

        }
    }

    @Override
    public void keyReleased(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_Q:
                System.out.println("Q released");
                off(30);
                break;
            case KeyEvent.VK_W:
                off(35);
                break;
            case KeyEvent.VK_E:
                break;
            case KeyEvent.VK_R:
                break;
            case KeyEvent.VK_T:
                break;
            case KeyEvent.VK_Z:
                break;
            case KeyEvent.VK_U:
                break;
            case KeyEvent.VK_I:
                break;
            case KeyEvent.VK_O:
                break;
            case KeyEvent.VK_P:
                break;
            default:

        }
    }

    @Override
    public void keyTyped(KeyEvent evt) {
    }

    @Override
    public void open() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        sequencer.addMetaEventListener(this);
        
        MidiChannel midiChannels[] = synthesizer.getChannels();
        channels = new ChannelData[midiChannels.length];
        for (int i = 0; i < channels.length; i++) {
            channels[i] = new ChannelData(midiChannels[i], i);
        }
        cc = channels[0];
    }

    @Override
    public void close() {

//        if (startButton.getText().startsWith("Stop")) {
//            startButton.doClick(0);
//        }
        if (sequencer != null) {
            sequencer.close();
        }
        sequencer = null;
    }

    @Override
    public void meta(MetaMessage message) {
        if (message.getType() == 47) {  // 47 is end of track
//            if (loopButton.getBackground().equals(Color.gray)) {
//                if (sequencer != null && sequencer.isOpen()) {
//                    buildTrackThenStartSequencer();
//                    sequencer.setTempoInBPM(bpmScroll.getValue());
//                }
//            } else {
//                startButton.setText("Start");
//            }
//            startButton.setText("Start");
//            statusPane.setText("STATUS: Stopped.");
        }
    }

    /**
     * Stores MidiChannel information.
     */
    class ChannelData {

        MidiChannel channel;
        boolean solo, mono, mute, sustain;
        int velocity, pressure, bend, reverb;
        int row, col, num;

        public ChannelData(MidiChannel channel, int num) {
            this.channel = channel;
            this.num = num;
            velocity = pressure = bend = reverb = 64;
        }

        public void setComponentStates() {
            table.setRowSelectionInterval(row, row);
            table.setColumnSelectionInterval(col, col);

//            soloCB.setSelected(solo);
//            monoCB.setSelected(mono);
//            muteCB.setSelected(mute);
//            //sustCB.setSelected(sustain);

//            JSlider slider[] = { veloS, presS, bendS, revbS };
//            int v[] = { velocity, pressure, bend, reverb };
//            for (int i = 0; i < slider.length; i++) {
//                TitledBorder tb = (TitledBorder) slider[i].getBorder();
//                String s = tb.getTitle();
//                tb.setTitle(s.substring(0, s.indexOf('=')+1)+s.valueOf(v[i]));
//                slider[i].repaint();
//            }
        }
    }

    public static void main(String args[]) {
        final SynthTest keySynth = new SynthTest();
        JFrame f = new JFrame("Track App");

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.getContentPane().add("Center", keySynth);
        f.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int w = 740;
        int h = 440;
        f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
        f.setSize(w, h);
        f.show();
        keySynth.open();

    }
}
