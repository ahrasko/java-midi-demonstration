/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import com.sun.media.sound.AudioSynthesizer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.*;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 *
 * @author Andrej Hraško
 */
public class Midi2WavTest {

//    Synthesizer synthesizer;
    public Midi2WavTest() {
    }

    public static void main(String[] args) {
        Synthesizer synthesizer = null;
        try {
            synthesizer = MidiSystem.getSynthesizer();
        } catch (MidiUnavailableException ex) {
            Logger.getLogger(Midi2WavTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            File midi_file = new File("bass01.midi");
            if (!midi_file.exists()) {
                throw new FileNotFoundException();
            }
            Sequence sequence = MidiSystem.getSequence(midi_file);

            Soundbank soundbank = null;

            try {
                soundbank = MidiSystem.getSoundbank(new File("RolandOrchestralRythm.sf2"));
            } catch (InvalidMidiDataException | IOException ex) {
                Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
            }

            render(soundbank, sequence, new File("out.wav"));
            System.exit(0);

        } catch (InvalidMidiDataException | IOException e) {
            System.out.println(e.toString());
            System.out.println();
        }

        System.out.println("MIDI to WAVE Render: usages:");
        System.out.println("java Midi2WavRender <midi_file_in> <wave_file_out> <soundbank_file>");
        System.exit(1);
    }

    /*
     * Render sequence using selected or default soundbank into wave audio file.
     */
    public static void render(Soundbank soundbank, Sequence sequence,
            File output_file) {
        try {
            // Find available AudioSynthesizer.
            AudioSynthesizer synth = findAudioSynthesizer();
            if (synth == null) {
                System.out.println("No AudioSynhtesizer was found!");
                System.exit(1);
            }

            // Open AudioStream from AudioSynthesizer.
            AudioInputStream stream = synth.openStream(null, null);

            // Load user-selected Soundbank into AudioSynthesizer.
            if (soundbank != null) {
                Soundbank defsbk = synth.getDefaultSoundbank();
                if (defsbk != null) {
                    synth.unloadAllInstruments(defsbk);
                }
                synth.loadAllInstruments(soundbank);
            }

            // Play Sequence into AudioSynthesizer Receiver.
            double total = send(sequence, synth.getReceiver());

            // Calculate how long the WAVE file needs to be.
            long len = (long) (stream.getFormat().getFrameRate() * (total + 4));
            stream = new AudioInputStream(stream, stream.getFormat(), len);

            // Write WAVE file to disk.
            AudioSystem.write(stream, AudioFileFormat.Type.WAVE, output_file);

            // We are finished, close synthesizer.
            synth.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Send entiry MIDI Sequence into Receiver using timestamps.
     */
    public static double send(Sequence seq, Receiver recv) {
        float divtype = seq.getDivisionType();
        assert (seq.getDivisionType() == Sequence.PPQ);
        Track[] tracks = seq.getTracks();
        int[] trackspos = new int[tracks.length];
        int mpq = 500000;
        int seqres = seq.getResolution();
        long lasttick = 0;
        long curtime = 0;
        while (true) {
            MidiEvent selevent = null;
            int seltrack = -1;
            for (int i = 0; i < tracks.length; i++) {
                int trackpos = trackspos[i];
                Track track = tracks[i];
                if (trackpos < track.size()) {
                    MidiEvent event = track.get(trackpos);
                    if (selevent == null
                            || event.getTick() < selevent.getTick()) {
                        selevent = event;
                        seltrack = i;
                    }
                }
            }
            if (seltrack == -1) {
                break;
            }
            trackspos[seltrack]++;
            long tick = selevent.getTick();
            if (divtype == Sequence.PPQ) {
                curtime += ((tick - lasttick) * mpq) / seqres;
            } else {
                curtime = (long) ((tick * 1000000.0 * divtype) / seqres);
            }
            lasttick = tick;
            MidiMessage msg = selevent.getMessage();
            if (msg instanceof MetaMessage) {
                if (divtype == Sequence.PPQ) {
                    if (((MetaMessage) msg).getType() == 0x51) {
                        byte[] data = ((MetaMessage) msg).getData();
                        mpq = ((data[0] & 0xff) << 16)
                                | ((data[1] & 0xff) << 8) | (data[2] & 0xff);
                    }
                }
            } else {
                if (recv != null) {
                    recv.send(msg, curtime);
                }
            }
        }
        return curtime / 1000000.0;
    }

    /*
     * Find available AudioSynthesizer.
     */
    public static AudioSynthesizer findAudioSynthesizer() {
        // First check if default synthesizer is AudioSynthesizer.
        Synthesizer synth;
        try {
            synth = MidiSystem.getSynthesizer();

            if (synth instanceof AudioSynthesizer) {
                return (AudioSynthesizer) synth;
            }

            // If default synhtesizer is not AudioSynthesizer, check others.
            Info[] infos = MidiSystem.getMidiDeviceInfo();
            for (int i = 0; i < infos.length; i++) {
                MidiDevice dev = MidiSystem.getMidiDevice(infos[i]);
                if (dev instanceof AudioSynthesizer) {
                    return (AudioSynthesizer) dev;
                }
            }
        } catch (MidiUnavailableException ex) {
            Logger.getLogger(Midi2WavTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        // No AudioSynthesizer was found, return null.
        return null;
    }
}
