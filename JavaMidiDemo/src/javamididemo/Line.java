/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrej Hraško
 */
public class Line {
    //list of instruments represented by Data objects
    List<Data> instruments;
    public String name;
    public List<Integer> startPositions;
    
    public Line(List<Data> instruments, String name){
        this.instruments = instruments;
        this.name = name;
        startPositions = new ArrayList();
    }

    public List<Data> getInstruments() {
        return instruments;
    }
            
    public int getInstrumentId(int i){
        return instruments.get(i).instrId;  
    }
    
    public int getInstrumentVelocity(int i){
        return instruments.get(i).velocity;  
    }
    
    public int getInstrumentNote(int i){
        return instruments.get(i).note;  
    }
    //name ziskam pri load() z nazvu suboru
    public String getName(){
        return name;
    }
    
    public List getStartPositions(){
        return startPositions;
    }
    
    public void setStartPosition(int position){
        startPositions.add(position);
    }
    
    public int getMaxTick(){
        int temp = 0;
        for (int i = 0; i< instruments.size(); i++){
            if(instruments.get(i).getLastTick() > temp){
                temp = instruments.get(i).getLastTick();
            }
        }
        return temp;
    }
}
