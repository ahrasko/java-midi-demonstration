/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

/**
 *
 * @author Jozko
 */
import com.sun.media.sound.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.Patch;
import javax.sound.sampled.*;

public class MakeSoundfont {

    public SF2Soundbank CreateSoundbank(String [] fnames)
            throws UnsupportedAudioFileException, IOException {

        SF2Soundbank sf2 = new SF2Soundbank();

        

        SF2Layer layer = new SF2Layer(sf2);
        layer.setName("fname Layer");
        sf2.addResource(layer);


        int i = 0;
        for (String fname : fnames) {

            File audiofile = new File("./" + fname);
            AudioInputStream audiostream = AudioSystem
                    .getAudioInputStream(audiofile);

            AudioFormat format = new AudioFormat(audiostream.getFormat()
                    .getSampleRate(), 16, 2, true, false);
            AudioInputStream convaudiostream = AudioSystem.getAudioInputStream(
                    format, audiostream);

            /*
             * Read the content of the file into a byte array.
             */

            int datalength = (int) convaudiostream.getFrameLength()
                    * format.getFrameSize();
            byte[] data = new byte[datalength];
            convaudiostream.read(data, 0, data.length);
            audiostream.close();

            /*
             * Create SoundFont2 sample.
             */

            SF2Sample sample = new SF2Sample(sf2);
            sample.setName(fname);
            sample.setData(data);
            sample.setSampleRate((long) format.getSampleRate());
            sample.setOriginalPitch(60 + i);
            sf2.addResource(sample);
            i++;

            /*
             * Create region for layer.
             */
            SF2LayerRegion region = new SF2LayerRegion();
            region.putInteger(SF2Region.GENERATOR_RELEASEVOLENV, 12000);
            region.putBytes(SF2Region.GENERATOR_KEYRANGE, new byte[]{(byte) (60 + i), (byte) (60 + i)});
            i++;
            region.setSample(sample);
            layer.getRegions().add(region);

//                SF2LayerRegion region = new SF2LayerRegion();
//                region.putInteger(SF2Region.GENERATOR_RELEASEVOLENV, 12000);
//                region.setSample(sample);
//
//                layer.getRegions().add(region);
        }

        /*
         * Create SoundFont2 instrument.
         */
        SF2Instrument ins = new SF2Instrument(sf2);
        ins.setName("Back Instrument");
        ins.setPatch(new Patch(0, 0));
        sf2.addInstrument(ins);

        /*
         * Create region for instrument.
         */
        SF2InstrumentRegion insregion = new SF2InstrumentRegion();
        insregion.setLayer(layer);
        ins.getRegions().add(insregion);

        return sf2;

    }
    
    public static void main(String[] args){
        
        String[] fnames = new String[]{"C.wav", "Cs.wav", "D.wav", "Ds.wav"};
        
        MakeSoundfont msf = new MakeSoundfont();
        try {
            msf.CreateSoundbank(fnames);
            
            
            
        } catch (UnsupportedAudioFileException | IOException ex) {
            Logger.getLogger(MakeSoundfont.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        }
        
        
    }
}