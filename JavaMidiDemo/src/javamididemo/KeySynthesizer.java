/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;
import javax.sound.midi.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 *
 * @author Andrej Hraško
 */
public class KeySynthesizer extends JPanel implements ControlContext, KeyListener, ActionListener, ChangeListener, MetaEventListener {

    final int PROGRAM_CHANGE = 192;
    final int NOTE_ON = 144;
    final int NOTE_OFF = 128;
    Synthesizer synthesizer;
    Sequencer sequencer;
    Sequence sequence;
    WavRenderer wawRenderer;
    MidiChannel midiChannels[];
    MidiChannel currentChannel;
    Instrument instruments[];
    Soundbank soundbank = null;
    Track track;
    List<Data> data;
    JTable table;
    JTextPane statusPane;
    JFrame frame;
    JTextField noteRangeFrom, noteRangeTo, fileName;
    JCheckBox sustainCheck, loopCheck;
    TableModel dataModel;
    JComboBox<Data> instrumentComboBox;
    JSlider velocitySlider;
    JScrollBar velocityScroll;
    JButton recordButton, playButton, stopButton;
    int velocity, activeInstrument, row, column, range[];
    long startTime;
    boolean recording, sustain = false;
    String frameName;
    int firstLine[] = {49, 50, 51, 52, 53, 54, 55, 56, 57, 48};
    int secondLine[] = {81, 87, 69, 82, 84, 90, 85, 73, 79, 80};
    int thirdLine[] = {65, 83, 68, 70, 71, 72, 74, 75, 76};
    int fourthLine[] = {89, 88, 67, 86, 66, 78, 77, 44, 46, 45};
    String[][] keyNames = new String[][]{
        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"},
        {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"},
        {"A", "S", "D", "F", "G", "H", "J", "K", "L", ""},
        {"Z", "X", "C", "V", "B", "N", "M", "<", ">", "_"}
    }, backupNames = new String[][]{
        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"},
        {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"},
        {"A", "S", "D", "F", "G", "H", "J", "K", "L", ""},
        {"Z", "X", "C", "V", "B", "N", "M", "<", ">", "_"}
    };

    public KeySynthesizer() {

        range = new int[4];
        velocity = 100;
        wawRenderer = new WavRenderer();

        try {
            if (synthesizer == null) {
                if ((synthesizer = MidiSystem.getSynthesizer()) == null) {
                    System.out.println("getSynthesizer() failed!");
                    return;
                }
            }
            synthesizer.open();
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
        open();
        //instrumenty
        soundbank = synthesizer.getDefaultSoundbank();
        if (soundbank != null) {
            instruments = soundbank.getInstruments();
            synthesizer.loadInstrument(instruments[0]);
        }

        data = new ArrayList(instruments.length);

        for (int i = 0; i < 4; i++) {
            data.add(new Data(i, instruments[i].getName(), i, 0, 30, 100));
            range[i] = 5;
        }

        synthesizer.loadInstrument(instruments[0]);

        //dostupne kanale
        midiChannels = synthesizer.getChannels();
        currentChannel = midiChannels[1];

        currentChannel.programChange(0);
        activeInstrument = 0;

        setLayout(new BorderLayout(5, 0));
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
        setBorder(eb);

        final String[] names = {"Instrument", "1", "2", "3", "4",
            "5", "6", "7", "8", "9", "10"};

        dataModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return names.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    return ((Data) data.get(row)).name;
                } else {
                    return keyNames[row][col - 1];
                }
            }

            @Override
            public String getColumnName(int col) {
                return names[col];
            }

            @Override
            public Class getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }

            @Override
            public void setValueAt(Object value, int row, int col) {
                if (col == 0) {
                    ((Data) data.get(row)).name = (String) value;
                } else {
                    keyNames[row][col - 1] = (String) value;
                }
            }
        };
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            @Override
            public void setValue(Object value) {
                setText((String) value);
            }
        };
        //creating new table with defined settings
        table = new JTable(dataModel);
        table.addKeyListener(this);
        table.getColumn(names[0]).setMinWidth(120);
        TableColumnModel tableCM = table.getColumnModel();
        for (int i = 1;
                i < names.length;
                i++) {
            TableColumn col = tableCM.getColumn(i);
            col.setCellRenderer(renderer);
        }
        table.setRowHeight(45);
        table.setTableHeader(null);

        // Listener for row changes
        ListSelectionModel lsm = table.getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            row = sm.getMinSelectionIndex();
                            loadInstrumentData();
                        }
                    }
                });
        // Listener for column changes
        lsm = table.getColumnModel().getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            column = sm.getMinSelectionIndex();
                        }
                        if (column != 0) {
                            Color c = ((Data) data.get(row)).staff[column - 1];
                            if (c.equals(Color.white)) {
                                ((Data) data.get(row)).staff[column - 1] = Color.black;
                            } else {
                                ((Data) data.get(row)).staff[column - 1] = Color.white;
                            }
                            table.tableChanged(new TableModelEvent(dataModel));
                        }
                    }
                });

        JPanel mainPanel = new JPanel();
        mainPanel.addKeyListener(this);
        SoftBevelBorder sbb = new SoftBevelBorder(BevelBorder.RAISED);
        mainPanel.setLayout(
                new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(Box.createVerticalStrut(10));

        //menu bar
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(menu);
        JMenu infoMenu = new JMenu("Info");
        infoMenu.setMnemonic(KeyEvent.VK_I);
        menuBar.add(infoMenu);

        JMenuItem about = new JMenuItem("About");
        about.setToolTipText("Info about the app.");
        about.addActionListener(this);
        infoMenu.add(about);

        JMenuItem menuMidiExport = new JMenuItem("Export MIDI", KeyEvent.VK_M);
        menu.add(menuMidiExport);
        menuMidiExport.setToolTipText("Exports actual recorded track into MIDI file.");
        menuMidiExport.addActionListener(this);
        JMenuItem menuWavExport = new JMenuItem("Export WAV", KeyEvent.VK_W);
        menu.add(menuWavExport);
        menuWavExport.setToolTipText("Exports actual recorded track into WAV file.");
        menuWavExport.addActionListener(this);
        JMenuItem menuImport = new JMenuItem("Import soundbank", KeyEvent.VK_I);
        menu.add(menuImport);
        menuImport.setToolTipText("Imports custom soundbank. (.sf2 or .dls)");
        menuImport.addActionListener(this);
        JMenuItem menuDefaultSB = new JMenuItem("Set default SB", KeyEvent.VK_D);
        menu.add(menuDefaultSB);
        menuDefaultSB.setToolTipText("Sets back the default soundbank.");
        menuDefaultSB.addActionListener(this);
        JMenuItem menuExit = new JMenuItem("Close", KeyEvent.VK_X);
        menuExit.setToolTipText("Coses the app.");
        menuExit.addActionListener(this);
        menu.add(menuExit);

        //second panel
        JPanel secondPanel = new JPanel();
        secondPanel.setLayout(new BoxLayout(secondPanel, BoxLayout.X_AXIS));

        //checkboxes
        JPanel checkPanel = new JPanel();
        checkPanel.add(sustainCheck = new JCheckBox("Sustain"));
        sustainCheck.addActionListener(this);
        sustainCheck.setFocusable(false);
        checkPanel.add(loopCheck = new JCheckBox("Play & Record"));
        loopCheck.addActionListener(this);
        loopCheck.setFocusable(false);

        //buttons
        JPanel buttonPanel = new JPanel(new GridLayout(3, 2));
        buttonPanel.setFocusable(false);
        buttonPanel.addKeyListener(this);
        buttonPanel.setBorder(new CompoundBorder(sbb, eb));
        buttonPanel.add(createButton("Edit Instrument", getBackground()));
        buttonPanel.add(recordButton = createButton("Record", getBackground()));
        buttonPanel.add(playButton = createButton("Play", getBackground()));
        buttonPanel.add(new JLabel());
//        buttonPanel.add(stopButton = createButton("Stop", getBackground()));
//        stopButton.setEnabled(false);
        buttonPanel.add(velocitySlider = new JSlider(JSlider.HORIZONTAL, 0, 127, 80));
        TitledBorder tb = new TitledBorder(new EtchedBorder());
        tb.setTitle("Velocity");
        velocitySlider.setBorder(tb);
        velocitySlider.setFocusable(false);
        velocitySlider.addChangeListener(this);
        buttonPanel.add(checkPanel);

        //instr
        JPanel instrumentPanel = new JPanel(new GridLayout(3, 2));
        instrumentPanel.setFocusable(false);
        instrumentPanel.setBorder(new CompoundBorder(sbb, eb));
        instrumentPanel.add(new JLabel("Instrument"));
        instrumentComboBox = createComboBox();
        instrumentComboBox.setRenderer(new InstrumentCellRenderer());
        instrumentComboBox.addActionListener(this);
        instrumentPanel.add(instrumentComboBox);
        instrumentPanel.add(new JLabel("Note range [0..127]"));
        instrumentPanel.add(new JLabel(""));
        instrumentPanel.add(noteRangeFrom = new JTextField());
        noteRangeFrom.addActionListener(this);
        instrumentPanel.add(noteRangeTo = new JTextField());
        noteRangeTo.addActionListener(this);

        statusPane = new JTextPane();
        statusPane.setText("WELCOME");
        statusPane.setEditable(false);

        secondPanel.add(buttonPanel);
        secondPanel.add(instrumentPanel);

        add("North", menuBar);
        mainPanel.add(secondPanel);
        add("South", mainPanel);
        mainPanel.add(statusPane);
        add("Center", new JScrollPane(table));


    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        KeySynthesizer keyS = new KeySynthesizer();

        JFrame f = new JFrame("Track App");
        f.addKeyListener(keyS);

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.getContentPane().add("Center", keyS);
        f.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int w = 640;
        int h = 480;
        f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
        f.setSize(w, h);
        f.show();
        keyS.open();

    }

    /**
     * given 120 bpm: (120 bpm) / (60 seconds per minute) = 2 beats per second 2
     * / 1000 beats per millisecond (2 * resolution) ticks per second (2 *
     * resolution)/1000 ticks per millisecond, or (resolution / 500) ticks per
     * millisecond ticks = milliseconds * resolution / 500
     */
    public void createEvent(int type, int value) {
        ShortMessage message = new ShortMessage();
        try {
            long millis = System.currentTimeMillis() - startTime;
            long tick = millis * sequence.getResolution() / 500;
            message.setMessage(type, value, velocity);
            MidiEvent event = new MidiEvent(message, tick);
            track.add(event);
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
    }

    /**
     * Plays one note on currently set MIDI channel.
     * Creates NoteOn event if recording = true.
     * @param note Note value.
     * @param instrId Used instrument.
     */
    private void playNote(int note, int instrId) {
        currentChannel.noteOn(note, velocity);
        if (recording) {
            createEvent(PROGRAM_CHANGE, instrId);
            createEvent(NOTE_ON, note);
        }
    }

    /**
     * Stops note currently played note on currently set MIDI channel.
     *
     * @param note Note Value.
     */
    private void stopNote(int note) {
        currentChannel.noteOff(note, velocity);
        if (recording) {
            createEvent(NOTE_OFF, note);
        }
    }

    /**
     * Sets instrument from currently set soundbank on currently set MIDI
     * channel.
     *
     * @param i Index of the instrument in the current soundbank.
     */
    private void setInstrument(int i) {
        if (!(activeInstrument == i)) {
            try {
                synthesizer.loadInstrument(instruments[i]);

                currentChannel.programChange(i);
                activeInstrument = i;
            } catch (ArrayIndexOutOfBoundsException ex) {
                statusPane.setText("ERROR: Instrument not loaded.");
            }
        }
    }

    /**
     * Changes the value in given cell. 
     * Occurs on KeyPress and KeyReleased
     *
     * @param t New value.
     * @param row Given row.
     * @param column Given column.
     */
    private void repaintCell(String t, int row, int column) {
        table.setValueAt(t, row, column + 1);
        table.tableChanged(new TableModelEvent(dataModel));
    }

    /**
     * Decides whether given list holds given object. Used to decide which key
     * was pressed or released.
     *
     * @param code Given key code.
     * @param list Final array of key codes.
     * @return Position of given code in the list. -1 if the list doesnt hold
     * the code.
     */
    private int contains(int code, int[] list) {
        for (int i = 0; i < list.length; i++) {
            if (list[i] == code) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object object = e.getSource();
        //checkbox actions
        if (object instanceof JCheckBox) {
            JCheckBox ch = (JCheckBox) object;
            if (ch.equals(sustainCheck)) {
                sustain = ch.isSelected();
                if (sustain) {
                    currentChannel.controlChange(64, 127);
                    statusPane.setText("STATUS: sustain enabled ");
                } else {
                    currentChannel.controlChange(64, 0);
                    statusPane.setText("STATUS: sustain disabled");
                }
            }
            //menu actions
        } else if (object instanceof JMenuItem) {
            JMenuItem m = (JMenuItem) object;
            if (m.getText().equals("Close")) {
                System.exit(0);
            } else if (m.getText().equals("Import soundbank")) {
                File file = new File("user.dir");
                final JFileChooser fc = new JFileChooser(file);
                fc.setFileFilter(new javax.swing.filechooser.FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        }
                        String name = f.getName();
                        if (name.endsWith(".sf2") || name.endsWith(".SF2") || name.endsWith(".dls")) {
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public String getDescription() {
                        return ".sf2, .dls, .SF2";
                    }
                });
                int returnVal = fc.showOpenDialog(KeySynthesizer.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    file = fc.getSelectedFile();
                    loadSoundbank(file);
                    updateDisplayedInfo();
                    statusPane.setText("STATUS: " + file.getName() + " imported.");
                    System.out.println("Importing: " + file.getName());
                } else {
                    System.out.println("Open command cancelled by user.");
                }
            } else if (m.getText().equals("Export MIDI")) {
                frameName = "Export to MIDI";
                createFrame(frameName);
            } else if (m.getText().equals("Export WAV")) {
                frameName = "Export to WAV";
                createFrame(frameName);
            } else if (m.getText().equals("Set default SB")) {
                soundbank = synthesizer.getDefaultSoundbank();
                instruments = soundbank.getInstruments();
                statusPane.setText("STATUS: Default soundbank has been set.");
                updateDisplayedInfo();
            } else if (m.getText().equals("About")) {
                showAboutDialog();
            }
            //button actions
        } else if (object instanceof JButton) {
            JButton b = (JButton) object;
            if (b.getText().equals("Edit Instrument")) {
                editInstrument();
            } else if (b.equals(recordButton)) {
                recording = recordButton.getText().equals("Record");
                if (recording) {
                    if (loopCheck.isSelected()) {
                        try {
                            sequencer.open();
                            sequencer.setSequence(sequence);
                        } catch (MidiUnavailableException | InvalidMidiDataException ex) {
                            statusPane.setText("ERROR: " + ex);
                        }
                        sequencer.start();
                        statusPane.setText("STATUS:Playing and updating the track..");
                    } else {
                        try {
                            sequence = new Sequence(Sequence.PPQ, 4);
                        } catch (Exception ex) {
                            statusPane.setText("ERROR: " + ex);
                        }
                        track = sequence.createTrack();
                        statusPane.setText("STATUS:Recording new track.");
                    }
                    startTime = System.currentTimeMillis();
                    playButton.setEnabled(false);
                    stopButton.setEnabled(false);
                    recordButton.setText("Stop record");
                } else {
                    playButton.setEnabled(true);
                    stopButton.setEnabled(false);
                    recordButton.setText("Record");
                }
            } else if (b.equals(playButton)) {
                try {
                    sequencer.open();
                    sequencer.setSequence(sequence);
                } catch (MidiUnavailableException | InvalidMidiDataException ex) {
                    statusPane.setText("ERROR: " + ex);
                }
                sequencer.start();
                recordButton.setEnabled(false);
                playButton.setEnabled(false);
                stopButton.setEnabled(true);
            } else if (b.equals(stopButton)) {

                sequencer.stop();
                playButton.setEnabled(true);
                recordButton.setEnabled(true);
                stopButton.setEnabled(false);
            } else if (b.getText().equals("Export")) {
                System.out.println(fileName.getText());

                if (frameName.equals("Export to MIDI")) {

                    int[] allowedTypes = MidiSystem.getMidiFileTypes(sequence);
                    if (allowedTypes.length == 0) {
                        System.err.println("No supported MIDI file types.");
                        statusPane.setText("ERROR: No supported MIDI file types.");
                    } else {
                        try {
                            MidiSystem.write(sequence, allowedTypes[0], new File(fileName.getText() + ".midi"));
                            statusPane.setText("STATUS: Exported to MIDI.");
                        } catch (IOException ex) {
                            statusPane.setText("ERROR: " + ex);
                        }
                    }
                } else if (frameName.equals("Export to WAV")) {
                    try {
                        wawRenderer.render(soundbank, sequence, new File(fileName.getText() + ".wav"));
                    } catch (MidiUnavailableException | IOException ex) {
                        Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
                        statusPane.setText("ERROR: " + ex);
                    }
                }
            } else if (b.getText().equals("Cancel")) {
                frame.dispose();
                frame = null;
            }
        } else if (object instanceof JComboBox) {
            transferFocus();
            table.requestFocusInWindow();
        } else if (object instanceof JTextField) {
            transferFocus();
            table.requestFocusInWindow();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //test na klavesy 1..0
        int temp = contains(e.getKeyCode(), firstLine);
        if (temp > -1) {
            setInstrument(((Data) data.get(0)).instrId);
            int note = ((Data) data.get(0)).note;
            int i = range[0] * temp;
            playNote(note + i, ((Data) data.get(0)).instrId);
            repaintCell("PRESS", 0, temp);
        }
        //test na klavesy Q..P
        temp = contains(e.getKeyCode(), secondLine);
        if (temp > -1) {
            setInstrument(((Data) data.get(1)).instrId);
            int note = ((Data) data.get(1)).note;
            int i = range[1] * temp;
            playNote(note + i, ((Data) data.get(1)).instrId);
            repaintCell("PRESS", 1, temp);
        }
        //test na klavesy A..L
        temp = contains(e.getKeyCode(), thirdLine);
        if (temp > -1) {
            
            setInstrument(((Data) data.get(2)).instrId);
            int note = ((Data) data.get(2)).note;
            int i = range[2] * temp;
            playNote(note + i, ((Data) data.get(2)).instrId);
            repaintCell("PRESS", 2, temp);
        }
        //test na klavesy Y.._
        temp = contains(e.getKeyCode(), fourthLine);
        if (temp > -1) {
            setInstrument(((Data) data.get(3)).instrId);
            int note = ((Data) data.get(3)).note;
            int i = range[3] * temp;
            playNote(note + i, ((Data) data.get(3)).instrId);
            repaintCell("PRESS", 3, temp);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //test na klavesy 1..0
        int temp = contains(e.getKeyCode(), firstLine);
        if (temp > -1) {
            int note = ((Data) data.get(0)).note;
            int i = range[0] * temp;
            stopNote(note + i);
            repaintCell(backupNames[0][temp], 0, temp);
        }
        //test na klavesy Q..P
        temp = contains(e.getKeyCode(), secondLine);
        if (temp > -1) {
            int note = ((Data) data.get(1)).note;
            int i = range[1] * temp;
            stopNote(note + i);
            repaintCell(backupNames[1][temp], 1, temp);
        }
        //test na klavesy A..L
        temp = contains(e.getKeyCode(), thirdLine);
        if (temp > -1) {
            int note = ((Data) data.get(2)).note;
            int i = range[2] * temp;
            stopNote(note + i);
            repaintCell(backupNames[2][temp], 2, temp);
        }
        //test na klavesy Y.._
        temp = contains(e.getKeyCode(), fourthLine);
        if (temp > -1) {
            int note = ((Data) data.get(3)).note;
            int i = range[3] * temp;
            stopNote(note + i);
            repaintCell(backupNames[3][temp], 3, temp);
        }
    }

    /**
     * Unused method. Used for testing purposes,
     *
     * @param event Given key event.
     * @return String with information about occured key event.
     */
    private String getKeyInfo(KeyEvent event) {
        String result;

        char ch = event.getKeyChar();
        int code = event.getKeyCode();
        int loc = event.getKeyLocation();
        int id = event.getID();
        int exCode = event.getExtendedKeyCode();
        String text = event.getKeyText(code);
        result = "id: " + id + ", text: " + text + ";\n"
                + "code: " + code + ", extended: " + exCode + ";\n";
        return result;
    }

    /**
     * Updates selected instrument with new data. New data are obtained from
     * Components of the application.
     */
    public void editInstrument() {
        try {
            Data temp = (Data) instrumentComboBox.getSelectedItem();
            Data result = (Data) data.get(row);
            result.instrId = temp.instrId;
            result.note = Integer.parseInt(noteRangeFrom.getText());
            result.name = temp.name;
            int i = Integer.parseInt(noteRangeTo.getText()) - Integer.parseInt(noteRangeFrom.getText());
            range[row] = i / 9;
            data.set(row, result);
            table.tableChanged(new TableModelEvent(dataModel));
            table.requestFocusInWindow();
            statusPane.setText("STATUS: Instrument updated.");
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
            System.out.println(ex);
        }
    }

    /**
     * Load new data into displayed components.
     */
    private void loadInstrumentData() {
        Data temp = (Data) data.get(row);
        instrumentComboBox.setSelectedIndex(temp.instrId);
        noteRangeFrom.setText(Integer.toString(temp.note));
        noteRangeTo.setText(Integer.toString(temp.note + 9 * range[row]));
    }

    /**
     * Creates JButton using given atributes.
     *
     * @param name Button label.
     * @param c Button color.
     * @return JButton object.
     */
    private JButton createButton(String bName, Color c) {
        JButton b = new JButton(bName);
//        b.setPreferredSize(new Dimension(100, 30));
        b.setBackground(c);
        b.setFocusable(false);
        b.addActionListener(this);
        return b;
    }

    /**
     * Creates JComboBox.
     *
     * @return JComboBox object.
     */
    private JComboBox<Data> createComboBox() {
        JComboBox combo;
        ArrayList list = new ArrayList(instruments.length);
        int l = instruments.length > 128 ? 128 : instruments.length;
        for (int i = 0; i < l; i++) {
            list.add(new Data(i, instruments[i].getName(), i, 0, 50, 100));
        }
        combo = new JComboBox(list.toArray());
        combo.addActionListener(this);
        return combo;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider s = (JSlider) e.getSource();
        if (s.equals(velocitySlider)) {
            velocity = velocitySlider.getValue();
        }
    }

    /**
     * Loads custom soundbank from choosen file.
     *
     * @param file SF2 or DLS file.
     */
    public void loadSoundbank(File file) {
        try {
            soundbank = MidiSystem.getSoundbank(file);
        } catch (InvalidMidiDataException | IOException ex) {
            Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
            statusPane.setText("ERROR: Soundbank issue: " +ex);
        }
        if (synthesizer.isSoundbankSupported(soundbank)) {
            synthesizer.loadAllInstruments(soundbank);
        } else {
            statusPane.setText("ERROR: Soundbank not supported.");
        }
        instruments = null;
        instruments = soundbank.getInstruments();
//        instruments = synthesizer.getLoadedInstruments();
        //sout for inst info
        for (int i = 0; i < instruments.length; i++) {
            System.out.println(i + " , " + instruments[i].getName() + " , " + instruments[i]);
        }

    }

    /**
     * Updates information in instrumentCombobox and the table after new
     * soundbank was loaded.
     */
    public void updateDisplayedInfo() {
        instrumentComboBox.removeAllItems();
        //soundbank cannot contain more instruments than 128
        int l = instruments.length > 128 ? 128 : instruments.length;
        //inserting instruments from soundbank into combobox
        for (int i = 0; i < l; i++) {
            instrumentComboBox.addItem(new Data(i, instruments[i].getName(), i, 0, 50, 100));
        }
        
        if (instruments.length > 3) {
            for (int i = 0; i < 4; i++) {
                ((Data) data.get(i)).name = instruments[((Data) data.get(i)).instrId].getName();
            }
        } else if (instruments.length == 1) {
            ((Data) data.get(0)).name = instruments[0].getName();
        } else if (instruments.length == 2) {
            ((Data) data.get(0)).name = instruments[0].getName();
            ((Data) data.get(1)).name = instruments[1].getName();
        } else if (instruments.length == 3) {
            ((Data) data.get(0)).name = instruments[0].getName();
            ((Data) data.get(1)).name = instruments[1].getName();
            ((Data) data.get(2)).name = instruments[2].getName();
        }

        table.tableChanged(new TableModelEvent(dataModel));
    }

    /**
     * Dialog in the [About] Frame.
     */
    private static void showAboutDialog() {
        final String msg =
                "This App serves for recording your own tracks using keyboard keys as synth keys.\n\n"
                + "Playing: \n"
                + "Just choose instrument you like from the combobox, row you want to put it in,\n "
                + "set the note range and press [Edit Instrument]. The key line is ready to rock.\n\n"
                + "Recording:\n"
                + "To record your playing, just press the [Record] button and hit the keyboard.\n"
                + "Recording will be stopped by the same button. If you want to add another sound\n"
                + "to your track, check the [Play & Record] option and the track won't be overwritten\n"
                + "but it will record new sounds into the old track while playing it.\n\n"
                + "Exporting:\n"
                + "After choosing to [Export] to MIDI or WAV just write the desired name and the song\n"
                + "will be rendered and saved in the given directory.";
        new Thread(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(null, msg, "Applet Info", JOptionPane.INFORMATION_MESSAGE);
            }
        }).start();
    }

    /**
     * Custom cell renderer for used combobox.
     */
    class InstrumentCellRenderer implements ListCellRenderer {

        protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
        private final Dimension preferredSize = new Dimension(0, 20);

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {
            JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                    isSelected, cellHasFocus);
            if (value instanceof Instrument) {

                renderer.setText(value.toString());
            }
            renderer.setPreferredSize(preferredSize);
            return renderer;
        }
    }

    @Override
    public final void open() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequence = new Sequence(Sequence.PPQ, 10);
        } catch (MidiUnavailableException | InvalidMidiDataException ex) {
            Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
            statusPane.setText("ERROR: " + ex);
            return;
        }
        sequencer.addMetaEventListener(this);
    }

    @Override
    public void close() {
        if (synthesizer != null) {
            synthesizer.close();
        }
        if (sequencer != null) {
            sequencer.close();
        }
        sequencer = null;
        synthesizer = null;
        instruments = null;
        midiChannels = null;
        currentChannel = null;
    }

    @Override
    public void meta(MetaMessage message) {
        if (message.getType() == 47) {  // 47 is end of track
            sequencer.setTickPosition(0);
            playButton.setEnabled(true);
            recordButton.setEnabled(true);
            stopButton.setEnabled(false);
            statusPane.setText("STATUS: Stopped.");
        }
    }

    /**
     * Creates frame for exporting files.
     *
     * @param titleName Name of the frame.
     */
    public void createFrame(String titleName) {
        int w = 500;
        int h = 130;
        JPanel panel = new JPanel(new BorderLayout());
        JPanel p1 = new JPanel();

        p1.add(new JLabel("File or Dir :"));
        String sep = String.valueOf(System.getProperty("file.separator").toCharArray()[0]);
        String text = null;
        try {
            text = System.getProperty("user.dir") + sep;
        } catch (SecurityException ex) {
            statusPane.setText(ex.toString());
            return;
        }
        fileName = new JTextField(text);
        fileName.setPreferredSize(new Dimension(w - 100, 30));
        fileName.addActionListener(this);

        p1.add(fileName);
        panel.add(p1);
        JPanel p2 = new JPanel();
        JButton applyB = createButton("Export", getBackground());
        p2.add(applyB);
        p2.add(createButton("Cancel", getBackground()));
        panel.add("South", p2);
        frame = new JFrame(titleName);
        frame.getContentPane().add("Center", panel);
        frame.pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(d.width / 2 - w / 2, d.height / 2 - h / 2);
        frame.setSize(w, h);
        frame.setVisible(true);
    }
}
