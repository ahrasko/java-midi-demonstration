/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.awt.Color;

/**
 *  Storage class for instrument and musical staff represented by color.
 * 
 * @author Andrej Hraško
 */
class Data extends Object {

    String name;
    int instrId;
    int note;
    int id;
    int lastTick;
    int velocity;
    Color staff[] = new Color[20];

    public int getInstrId() {
        return instrId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNote() {
        return note;
    }

    public int getVelocity() {
        return velocity;
    }

    public Color[] getStaff() {
        return staff;
    }

    public void setStaff(Color[] staff) {
        this.staff = staff;
    }

    public void id(int id) {
        this.id = id;
    }

    public int getLastTick() {
        return lastTick;
    }

    public void setLastTick(int lastTick) {
        this.lastTick = lastTick;
    }

    @Override
    public String toString() {
        return instrId + " " + name;
    }
 
    public Data(int instrId, String name, int id, int lastTick, int note, int velocity) {
        this.name = name;
        this.velocity = velocity;
        this.instrId = instrId;
        this.id = id;
        this.lastTick = lastTick;
        this.note = note;
        for (int i = 0; i < staff.length; i++) {
            staff[i] = Color.white;
        }
    }
}
