
package javamididemo;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 * Application for building songs using tracks created in the Groove App.
 * 
 * @author Andrej Hraško
 */
public class SongBuilder extends JPanel implements ActionListener, ControlContext, MetaEventListener, ChangeListener {

    final int PROGRAM_CHANGE = 192;
    final int NOTE_ON = 144;
    final int NOTE_OFF = 128;
    boolean maxTickNum = true;
    DatabaseHandler handler;
    WavRenderer wavRenderer;
    Sequencer sequencer;
    Sequence sequence;
    Synthesizer synthesizer;
    Soundbank soundbank;
    Track track;
    TableModel dataModel;
    JTable table;
    JFrame frame;
    JTextPane infoPane;
    JTextField fileName, trackName;;
    JSlider bpmSlider;
    JButton startButton, addButton, editButton, deleteButton, loopButton;
    JTextPane statusPane;
    ArrayList song, columnNames;
    int row, column;
    Instrument instruments[];
    String frameName;

    public SongBuilder() {

        try {
            if (synthesizer == null) {
                synthesizer = MidiSystem.getSynthesizer();
            }
        } catch (Exception ex) {
            Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        open();
        wavRenderer = new WavRenderer();
        handler = new DatabaseHandler();

        instruments = synthesizer.getDefaultSoundbank().getInstruments();
        soundbank = synthesizer.getDefaultSoundbank();
        song = new ArrayList();
        columnNames = new ArrayList();

        columnNames.add("Track");
        for (int i = 1; i < 301; i++) {
            columnNames.add(Integer.toString(i));
        }

        setLayout(new BorderLayout(5, 0));
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
        setBorder(eb);

        dataModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return columnNames.toArray().length;
            }

            @Override
            public int getRowCount() {
                return song.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    return ((Line) song.get(row)).getName();
                } else {
                    List positions = ((Line) song.get(row)).getStartPositions();
                    if (positions == null) {
                        return Color.WHITE;
                    }
                    if (positions.contains(col)) {
                        return Color.BLACK;
                    } else {
                        return Color.WHITE;
                    }
                }
            }

            @Override
            public String getColumnName(int col) {
                return (String) columnNames.get(col);
            }

            @Override
            public Class getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int row, int col) {
                if (col == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public void setValueAt(Object value, int row, int col) {
                if (col == 0) {
//                    ((Data) song.get(row)).name = (String) value;
                    ((Line) song.get(row)).name = (String) value;
                } else {
//                    ((Data) song.get(row)).staff[col - 1] = (Color) value;
                    ((Line) song.get(row)).setStartPosition(col);
                }
            }
        };
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            @Override
            public void setValue(Object value) {
                setBackground((Color) value);
            }
        };

        //creating new table with defined settings
        table = new JTable(dataModel);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setPreferredSize(new Dimension(7000, 208));
        table.getColumn(columnNames.get(0)).setMinWidth(100);
        TableColumnModel tableCM = table.getColumnModel();
        for (int i = 1; i < columnNames.toArray().length; i++) {
            TableColumn col = tableCM.getColumn(i);
            col.setCellRenderer(renderer);
        }
        // Listener for row changes
        ListSelectionModel lsm = table.getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            row = sm.getMinSelectionIndex();
                        }
                        loadTrackData();
                    }
                });
        // Listener for column changes
        lsm = table.getColumnModel().getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            column = sm.getMinSelectionIndex();
                        }
                        if (column != 0) {
                            List positions = ((Line) song.get(row)).getStartPositions();

                            if (positions.contains(column)) {
                                positions.remove((Integer) column);
                            } else {
                                positions.add(column);
                            }
                            table.tableChanged(new TableModelEvent(dataModel));
                        }
                    }
                });

        JPanel mainPanel = new JPanel();

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        SoftBevelBorder sbb = new SoftBevelBorder(BevelBorder.RAISED);
        mainPanel.add(Box.createVerticalStrut(10));

        // menu bar
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menu.getAccessibleContext().setAccessibleDescription("file menu");
        menuBar.add(menu);

        JMenu infoMenu = new JMenu("Info");
        infoMenu.setMnemonic(KeyEvent.VK_I);
        menuBar.add(infoMenu);
        // JMenu Items
        JMenuItem about = new JMenuItem("About");
        about.setToolTipText("Info about the app.");
        about.addActionListener(this);
        infoMenu.add(about);
        JMenuItem exportMIDI = new JMenuItem("Export MIDI", KeyEvent.VK_M);
        exportMIDI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
        menu.add(exportMIDI);
        exportMIDI.addActionListener(this);
        JMenuItem exportWAV = new JMenuItem("Export WAV", KeyEvent.VK_W);
        exportWAV.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
        menu.add(exportWAV);
        exportWAV.addActionListener(this);
        JMenuItem menuExit = new JMenuItem("Close", KeyEvent.VK_L);
        menuExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        menuExit.getAccessibleContext().setAccessibleDescription("This will close the app.");
        menu.add(menuExit);

        // buttons
        JPanel buttonPanel = new JPanel(new GridLayout(4, 3));
        buttonPanel.setBorder(new CompoundBorder(sbb, eb));
        buttonPanel.add(startButton = createButton("Start", getBackground()));
        buttonPanel.add(loopButton = createButton("Loop", getBackground()));
        buttonPanel.add(createButton("Clear Grid", getBackground()));
        buttonPanel.add(createButton("Clear Tracks", getBackground()));
        buttonPanel.add(addButton = createButton("Add Track", getBackground()));
        buttonPanel.add(deleteButton = createButton("Delete Track", getBackground()));
        buttonPanel.add(bpmSlider = new JSlider(JSlider.HORIZONTAL, 0, 127, 100));
        TitledBorder tb = new TitledBorder(new EtchedBorder());
        tb.setTitle("BPM");
        bpmSlider.setBorder(tb);
        bpmSlider.setFocusable(false);
        bpmSlider.addChangeListener(this);
        infoPane = new JTextPane();
        infoPane.setPreferredSize(new Dimension(20,20));
        buttonPanel.add(infoPane);
        
        JScrollPane tablePane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        statusPane = new JTextPane();
        statusPane.setEditable(false);

        add("North", menuBar);
        mainPanel.add(buttonPanel);
        mainPanel.add(Box.createVerticalStrut(10));
        add("South", mainPanel);
        mainPanel.add(statusPane);
        add("Center", tablePane);


    }
/**
 * Builds track based on informations from the table and loaded tracks.
 * Rekursively runs trough all staff records of all Data objects in all
 * loaded tracks.
 */
    public void buildTrack() {
        try {
            sequence = new Sequence(Sequence.PPQ, 4);
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
        track = sequence.createTrack();

        for (int i = 0; i < song.size(); i++) {
            Line row = (Line) song.get(i);
            List<Data> instruments = row.getInstruments();
            List<Integer> positions = row.getStartPositions();
            for (int j = 0; j < positions.size(); j++) {
                for (int k = 0; k < instruments.size(); k++) {
                    Data instrument = instruments.get(k);
                    for (int l = 0; l < instrument.staff.length; l++) {
                        if (instrument.staff[l].equals(Color.BLACK)) {
                            createEvent(PROGRAM_CHANGE, 1, instrument.instrId, instrument.velocity, positions.get(j) + l);
                            createEvent(NOTE_ON, 1, instrument.note, instrument.velocity, positions.get(j) + l);
                            createEvent(NOTE_OFF, 1, instrument.note, instrument.velocity, positions.get(j) + l + 1);
                        }
                    }
                }
            }
        }
    }

    /**
     * Starts the sequencer after building complete track.
     */
    public void startSequencer() {
        buildTrack();
        // set and start the sequencer.
        try {
            sequencer.setSequence(sequence);
        } catch (InvalidMidiDataException ex) {
            statusPane.setText("ERROR: " + ex);
        }
        sequencer.start();
        sequencer.setTempoInBPM(bpmSlider.getValue());
    }
/**
 * Renders and exports MIDI file from constructed sequence;
 * @param filename Name of the generated MIDI file.
 */
    public void renderMIDI(String filename) {
        buildTrack();

        int[] allowedTypes = MidiSystem.getMidiFileTypes(sequence);
        if (allowedTypes.length == 0) {
            System.err.println("No supported MIDI file types.");
            statusPane.setText("ERROR: No supported MIDI file types.");
        } else {
            try {
                MidiSystem.write(sequence, allowedTypes[0], new File(filename + ".midi"));
            } catch (IOException ex) {
                statusPane.setText("ERROR: " + ex);
            }
        }
    }
    /**
     * Creates one MIDI Event based on given atributes.
     * Type of the event is determined by the [type] atribute.
     * PROGRAM_CHANGE will change loaded instrument.
     * NOTE_ON will play one note.
     * NOTE_OFF will stop currently played note.
     * @param type  Type of the event.
     * @param chan  Channel for which is event created.
     * @param num   Instrument id for PROGRAM_CHANGE, note for NOTE_ON/OFF.
     * @param velocity  Velocity of the event.
     * @param tick Tick when will the event occured.
     */
    public void createEvent(int type, int chan, int num, int velocity, long tick) {
        ShortMessage message = new ShortMessage();
        try {
            message.setMessage(type, chan, num, velocity);
            MidiEvent event = new MidiEvent(message, tick);
            track.add(event);
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
        }
    }

    public static void main(String args[]) {
        final SongBuilder songBuilder = new SongBuilder();
        JFrame f = new JFrame("Track App");

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.getContentPane().add("Center", songBuilder);
        f.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int w = 640;
        int h = 480;
        f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
        f.setSize(w, h);
        f.show();
        songBuilder.open();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object object = e.getSource();

        if (object instanceof JButton) {
            JButton b = (JButton) object;
            if (b.equals(startButton)) {
                if (b.getText().equals("Start")) {
                    startSequencer();
                    statusPane.setText("STATUS: Playing..");
                    b.setText("Stop");
                } else {
                    sequencer.stop();
                    b.setText("Start");
                }
            } else if (b.equals(loopButton)) {
                if (loopButton.getBackground().equals(Color.gray)) {
                    loopButton.setBackground(getBackground());
                } else {
                    loopButton.setBackground(Color.gray);
                }
            } else if (b.equals(addButton)) {
                File file = new File("trackDir/");
                final JFileChooser fc = new JFileChooser(file);
                fc.setFileFilter(new javax.swing.filechooser.FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        }
                        String name = f.getName();
                        if (name.endsWith(".db")) {
                            return true;
                        }
                        return false;
                    }
                    @Override
                    public String getDescription() {
                        return ".db";
                    }
                });
                int returnVal = fc.showOpenDialog(null);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    file = fc.getSelectedFile();
                    String s = file.getName();
                    int pos = s.lastIndexOf(".");
                    if (pos > 0) {
                        s = s.substring(0, pos);
                    }
                    ArrayList temp = handler.load(s);
                    Line line = new Line(temp, s);
                    song.add(line);
                    table.tableChanged(new TableModelEvent(dataModel));
                    statusPane.setText("STATUS: " + s + " imported.");
                    System.out.println("Importing: " + file.getName());
                } else {
                    System.out.println("Open command cancelled by user.");
                }
            } else if (b.equals(deleteButton)) {
                deleteTrack();
            } else if (b.getText().startsWith("Clear Grid")) {
                for (int i = 0; i < song.size(); i++) {
                    ((Line) song.get(i)).startPositions.clear();
                }
                table.tableChanged(new TableModelEvent(dataModel));
            } else if (b.getText().startsWith("Clear Tracks")) {
                song.clear();
                table.tableChanged(new TableModelEvent(dataModel));
            } else if (b.getText().equals("Export")) {
                if (frameName.equals("Export to MIDI")) {
                    buildTrack();
                    renderMIDI(fileName.getText());
                } else if(frameName.equals("Export to WAV")){
                    try {
                        buildTrack();
                        wavRenderer.render(soundbank, sequence, new File(fileName.getText() + ".wav"));
                    } catch (MidiUnavailableException | IOException ex) {
                        Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
                        statusPane.setText("ERROR: " + ex);
                    }
                }
                frame.dispose();
                frame = null;
            } else if (b.getText().equals("Cancel")) {
                frame.dispose();
                frame = null;
            }
        } else if (object instanceof JMenuItem) {
            JMenuItem m = (JMenuItem) object;
            if (m.getText().startsWith("Export MIDI")) {
                frameName = "Export to MIDI";
                createFrame(frameName);
                statusPane.setText("STATUS: MIDI track saved.");
            } else if (m.getText().startsWith("Export WAV")) {
                frameName = "Export to WAV";
                createFrame(frameName);
            } else if (m.getText().startsWith("Close")) {
                System.exit(0);
            } else if (m.getText().startsWith("About")) {
                showAboutDialog();
            }         
        } 
    }
/**
 * Removes track currently selected in the grid. 
 * If no track is selected, removes first track.
 */
    public void deleteTrack() {
        try {
            String name = ((Line) song.get(row)).getName();
            song.remove(song.get(row));
            table.clearSelection();
            table.tableChanged(new TableModelEvent(dataModel));
            statusPane.setText("STATUS: Track " + name + " removed.");
        } catch (Exception ex) {
            statusPane.setText("ERROR: " + ex);
            System.out.println(ex);
        }
    }
/**
 * Load new data into displayed components.
 */
    public void loadTrackData() {
        Line temp = (Line) song.get(row);
        int duration = temp.getMaxTick();
        int instNum = temp.getInstruments().size();
        infoPane.setText("Track info: \n" +"Duration - " +duration +"  ; Number of Instruments - " +instNum);
    }
    /**
     * Creates JButton using given atributes.
     * @param name Button label.
     * @param c Button color.
     * @return JButton object.
     */
    private JButton createButton(String name, Color c) {
        JButton b = new JButton(name);
        b.setBackground(c);
        b.addActionListener(this);
        return b;
    }
    /**
     * Creates frame for exporting files.
     *
     * @param titleName Name of the frame.
     */
    public void createFrame(String titleName) {
        int w = 500;
        int h = 130;
        JPanel panel = new JPanel(new BorderLayout());
        JPanel p1 = new JPanel();

        p1.add(new JLabel("File or Dir :"));
        String sep = String.valueOf(System.getProperty("file.separator").toCharArray()[0]);
        String text = null;
        try {
            text = System.getProperty("user.dir") + sep;
        } catch (SecurityException ex) {
            statusPane.setText(ex.toString());
            return;
        }
        fileName = new JTextField(text);
        fileName.setPreferredSize(new Dimension(w - 100, 30));
        fileName.addActionListener(this);

        p1.add(fileName);
        panel.add(p1);
        JPanel p2 = new JPanel();
        JButton applyB = createButton("Export", getBackground());
        p2.add(applyB);
        p2.add(createButton("Cancel", getBackground()));
        panel.add("South", p2);
        frame = new JFrame(titleName);
        frame.getContentPane().add("Center", panel);
        frame.pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(d.width / 2 - w / 2, d.height / 2 - h / 2);
        frame.setSize(w, h);
        frame.setVisible(true);
    }

    /**
     * Dialog in the [About] Frame.
     */
    private static void showAboutDialog() {
        final String msg =
                "This part builds songs from sample tracks created in the Groove Application.\n\n"
                + "Building songs: \n"
                + "To build a song first [Add] tracks into the grid. After that, you can select cells\n"
                + "in the rows to choose when will the coresponding track play. Mind the info about the\n"
                + "duration of tracks so the won't play one trough another.\n\n"
                + "Saving: \n"
                + "To save the song simply choose [Export ...] option in the [File] menu.\n"
                + "Come with catchy name, hit [Export] and done. Your song is ready to play.";
        new Thread(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(null, msg, "Applet Info", JOptionPane.INFORMATION_MESSAGE);
            }
        }).start();
    }
    
    @Override
    public final void open() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
        } catch (Exception ex) {
            Logger.getLogger(KeySynthesizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        sequencer.addMetaEventListener(this);
    }

    @Override
    public void close() {
        if (startButton.getText().equals("Stop")) {
            startButton.doClick(0);
        }
        if (sequencer != null) {
            sequencer.close();
        }
        sequencer = null;
    }

    @Override
    public void meta(MetaMessage meta) {
        if (meta.getType() == 47) {  // 47 is end of track
            if (loopButton.getBackground().equals(Color.gray)) {
                if (sequencer != null && sequencer.isOpen()) {
                    startSequencer();
                    sequencer.setTempoInBPM(bpmSlider.getValue());
                }
            } else {
                startButton.setText("Start");
                statusPane.setText("STATUS: Stopped.");
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider s = (JSlider) e.getSource();
        if (s.equals(bpmSlider)) {
            statusPane.setText("STATUS: BPM set to " +bpmSlider.getValue() +".");
        }
    }
}
