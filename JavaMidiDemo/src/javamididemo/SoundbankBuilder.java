/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import com.sun.media.sound.AudioFloatConverter;
import com.sun.media.sound.FFT;
import com.sun.media.sound.SF2Instrument;
import com.sun.media.sound.SF2InstrumentRegion;
import com.sun.media.sound.SF2Layer;
import com.sun.media.sound.SF2LayerRegion;
import com.sun.media.sound.SF2Region;
import com.sun.media.sound.SF2Sample;
import com.sun.media.sound.SF2Soundbank;
import com.sun.media.sound.SoftSynthesizer;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.*;
import javax.sound.sampled.AudioFormat;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.plaf.DimensionUIResource;
import javax.swing.table.*;

/**
 * Application for designing SF2 Instruments and saving those into SF2 Soundbank files.
 * 
 * @author Andrej Hraško
 */
public class SoundbankBuilder extends JPanel implements ControlContext, KeyListener, ActionListener, ChangeListener {

    private Synthesizer synthesizer = new SoftSynthesizer();
    private Receiver receiver;
    SF2Soundbank soundbank, playSoundbank;
    List<SF2Instrument> instruments;
    List<List<Integer>> instrumentsGains;
    List<List<Integer>> instrumentsWidths;
    ArrayList<SFGeneratorSlider> generators = new ArrayList<>();
    JSlider[] width_sliders = new JSlider[20];
    JSlider[] gain_sliders = new JSlider[20];
    JLabel[] harmonic_field = new JLabel[20];
    JTextPane statusPane;
    JFrame frame;
    JButton instSave, instDelete, playB;
    JTextField instName, fileName, sbVendor;
    JTextArea sbDescription;
    JSlider velocitySlider;
    TableModel dataModel;
    JTable table;
    double[] data = null;
    double[] data_audio;
    double[] data_audio2;
    boolean stereo_mode = false, keyPressed = false;
    int row, column;
    myPanel sliderPanel;
    JPanel instrumentPanel;

    public SoundbankBuilder() {
        soundbank = new SF2Soundbank();
        playSoundbank = new SF2Soundbank();
        instruments = new ArrayList<>(128);
        instrumentsGains = new ArrayList<>();
        instrumentsWidths = new ArrayList<>();

        setLayout(new BorderLayout(5, 0));
        EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
        setBorder(eb);

        JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
        SoftBevelBorder sbb = new SoftBevelBorder(BevelBorder.RAISED);
        //menu bar
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(menu);
        JMenu infoMenu = new JMenu("Info");
        infoMenu.setMnemonic(KeyEvent.VK_I);
        menuBar.add(infoMenu);

        JMenuItem menuNew = new JMenuItem("New Soundbank", KeyEvent.VK_N);
        menu.add(menuNew);
        menuNew.setToolTipText("Clears soundbank.");
        menuNew.addActionListener(this);
        JMenuItem menuSave = new JMenuItem("Save Soundbank", KeyEvent.VK_S);
        menu.add(menuSave);
        menuSave.setToolTipText("Saves soundbank.");
        menuSave.addActionListener(this);
        JMenuItem menuExit = new JMenuItem("Close", KeyEvent.VK_X);
        menuExit.setToolTipText("Coses the app.");
        menuExit.addActionListener(this);
        menu.add(menuExit);




//        JPanel sliderPanel = new JPanel(new GridBagLayout());
        sliderPanel = new myPanel("scale.png");
        sliderPanel.setMinimumSize(new Dimension(1020, 288));
        sliderPanel.setLayout(new GridBagLayout());
        sliderPanel.addKeyListener(this);
        sliderPanel.setFocusable(true);
        sliderPanel.requestFocusInWindow();
        sliderPanel.setBorder(new CompoundBorder(sbb, eb));
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        for (int i = 0; i < width_sliders.length; i++) {
            JLabel label = new JLabel("" + (i + 1));
            JSlider amp_slider = createSlider(JSlider.VERTICAL);
            amp_slider.setMinimumSize(new Dimension(20, 180));
            amp_slider.addChangeListener(this);
            amp_slider.setFocusable(false);
            JSlider width_slider = createSlider(JSlider.HORIZONTAL);
            width_slider.setFocusable(false);
            width_slider.addChangeListener(this);
            width_slider.setMinimumSize(new Dimension(45, 20));

            width_sliders[i] = width_slider;
            gain_sliders[i] = amp_slider;
            harmonic_field[i] = label;
            c.gridy = 0;
            c.gridx = i + 1;
            sliderPanel.add(label, c);
            c.gridy = 1;
            sliderPanel.add(amp_slider, c);
            c.gridy = 2;
            sliderPanel.add(width_slider, c);
        }

        JPanel settingsPanel = new JPanel(new GridLayout(2, 2));
        settingsPanel.setBorder(new CompoundBorder(sbb, eb));
        settingsPanel.addKeyListener(this);

        JPanel volumeEnP = new JPanel(new GridLayout(8, 2));
        volumeEnP.add(new JLabel("Volume Envelope"));
        volumeEnP.add(new JLabel());
        volumeEnP.add(new JLabel("Delay"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_DELAYVOLENV));
        volumeEnP.add(new JLabel("Attack"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_ATTACKVOLENV));
        volumeEnP.add(new JLabel("Hold"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_HOLDVOLENV));
        volumeEnP.add(new JLabel("Decay"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_DECAYVOLENV));
        volumeEnP.add(new JLabel("Sustain"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_SUSTAINVOLENV));
        volumeEnP.add(new JLabel("Release"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_RELEASEVOLENV));
        volumeEnP.add(new JLabel("Attenuation"));
        volumeEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_INITIALATTENUATION));
        settingsPanel.add(volumeEnP);

        JPanel modEnP = new JPanel(new GridLayout(9, 2));
        modEnP.add(new JLabel("Modulation Envelope"));
        modEnP.add(new JLabel());
        modEnP.add(new JLabel("Delay"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_DELAYMODENV));
        modEnP.add(new JLabel("Attack"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_ATTACKMODENV));
        modEnP.add(new JLabel("Hold"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_HOLDMODENV));
        modEnP.add(new JLabel("Decay"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_DECAYMODENV));
        modEnP.add(new JLabel("Sustain"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_SUSTAINMODENV));
        modEnP.add(new JLabel("Release"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_RELEASEMODENV));
        modEnP.add(new JLabel("To Filter Cutoff"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_MODENVTOFILTERFC));
        modEnP.add(new JLabel("To Pitch"));
        modEnP.add(new SFGeneratorSlider(SF2Region.GENERATOR_MODENVTOPITCH));
        settingsPanel.add(modEnP);

        JPanel modLFOP = new JPanel(new GridLayout(5, 2));
        modLFOP.add(new JLabel("Modulation LFO"));
        modLFOP.add(new JLabel());
        modLFOP.add(new JLabel("Delay"));
        modLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_DELAYMODLFO));
        modLFOP.add(new JLabel("Frequency"));
        modLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_FREQMODLFO));
        modLFOP.add(new JLabel("To Filter Cutoff"));
        modLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_MODLFOTOFILTERFC));
        modLFOP.add(new JLabel("To Pitch"));
        modLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_MODLFOTOPITCH));
        settingsPanel.add(modLFOP);

        JPanel vibLFOP = new JPanel(new GridLayout(5, 2));
        vibLFOP.add(new JLabel("Vibration LFO"));
        vibLFOP.add(new JLabel());
        vibLFOP.add(new JLabel("Delay"));
        vibLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_DELAYVIBLFO));
        vibLFOP.add(new JLabel("Frequency"));
        vibLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_FREQVIBLFO));
        vibLFOP.add(new JLabel("To Pitch"));
        vibLFOP.add(new SFGeneratorSlider(SF2Region.GENERATOR_VIBLFOTOPITCH));

        JCheckBox stereoCheck = new JCheckBox("Stereo");
        stereoCheck.setFocusable(false);
        stereoCheck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stereo_mode = !stereo_mode;
                System.out.println(stereo_mode);
            }
        });
        vibLFOP.add(stereoCheck);

        vibLFOP.add(velocitySlider = new JSlider(JSlider.HORIZONTAL, 0, 127, 80));
        TitledBorder tb = new TitledBorder(new EtchedBorder());
        tb.setTitle("Velocity");
        velocitySlider.setBorder(tb);
        velocitySlider.setFocusable(false);
        velocitySlider.addChangeListener(this);
        velocitySlider.setMaximumSize(new Dimension(80, 30));
        velocitySlider.setPreferredSize(new Dimension(80, 30));
        settingsPanel.add(vibLFOP);

//        JPanel instrumentPanel = new JPanel();
        instrumentPanel = new JPanel();
        instrumentPanel.addKeyListener(this);
        instrumentPanel.setLayout(new BoxLayout(instrumentPanel, BoxLayout.Y_AXIS));
        instrumentPanel.setBorder(new CompoundBorder(sbb, eb));
        playB = createButton("PLAY", getBackground());
        instrumentPanel.add(playB);
        instrumentPanel.add(createButton("CLEAR", getBackground()));
        instSave = createButton("Save instrument", getBackground());
        instrumentPanel.add(instSave);
        instDelete = createButton("Delete instrument", getBackground());
        instrumentPanel.add(instDelete);
        instName = new JTextField("default name");
        instName.setMaximumSize(new Dimension(300, 30));
        instrumentPanel.add(instName);

        dataModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public int getRowCount() {
                return instruments.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                return ((SF2Instrument) instruments.get(row)).getName();
            }

            @Override
            public String getColumnName(int col) {
                return "Instruments";
            }

            @Override
            public Class getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }

            @Override
            public void setValueAt(Object value, int row, int col) {
                String instName = ((SF2Instrument) instruments.get(row)).getName();
                instName = (String) value;
            }
        };
        //creating new table with defined settings
        table = new JTable(dataModel);
        table.addKeyListener(this);
//        table.setm
        table.setRowHeight(45);
        table.setTableHeader(null);
        // Listener for row changes
        ListSelectionModel lsm = table.getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            row = sm.getMinSelectionIndex();
                            loadInstrumentData();
                        }
                    }
                });
        // Listener for column changes
        lsm = table.getColumnModel().getSelectionModel();

        lsm.addListSelectionListener(
                new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        ListSelectionModel sm = (ListSelectionModel) e.getSource();
                        if (!sm.isSelectionEmpty()) {
                            column = sm.getMinSelectionIndex();
                        }
                        if (column != 0) {
                            ((SF2Instrument) instruments.get(row)).getName();
                            table.tableChanged(new TableModelEvent(dataModel));
                        }
                    }
                });
        JScrollPane tablePane = new JScrollPane(table);
        tablePane.setMaximumSize(new Dimension(300, 200));
        instrumentPanel.add(tablePane);
//        instrumentPanel.add(new JScrollPane(table));


        mainPanel.add(sliderPanel, BorderLayout.PAGE_START);
        mainPanel.add(settingsPanel, BorderLayout.LINE_START);
        mainPanel.add(instrumentPanel, BorderLayout.CENTER);

        add("North", menuBar);
        add("Center", mainPanel);
        statusPane = new JTextPane();
        statusPane.setText("WELCOME");
        statusPane.setEditable(false);
        add("South", statusPane);
    }

    public JSlider createSlider(int orientation) {
        JSlider slider = new JSlider(orientation, 0, 100, 0);
        return slider;
    }

    /**
     * Creates JButton using given atributes.
     *
     * @param name Button label.
     * @param c Button color.
     * @return JButton object.
     */
    private JButton createButton(String bName, Color c) {
        JButton b = new JButton(bName);
        b.setMaximumSize(new Dimension(150, 30));
        b.setPreferredSize(new Dimension(150, 30));
        b.setBackground(c);
        b.setFocusable(false);
        b.addActionListener(this);
        return b;
    }

    @Override
    public void open() {
        try {
            synthesizer.open();
            receiver = synthesizer.getReceiver();
        } catch (MidiUnavailableException ex) {
            Logger.getLogger(SoundbankBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void close() {
        synthesizer.close();
    }

    /**
     * Starts playing of the given note. Note plays with the given pitch and
     * velocity from velocity slider.
     *
     * @param i Pitch of the note
     */
    private void playNote(int i) {
        final MidiChannel channel1 = synthesizer.getChannels()[0];
        channel1.controlChange(7, 127);
        synthesizer.unloadAllInstruments(playSoundbank);
        synthesizer.loadInstrument(designInstrument(playSoundbank));
        channel1.programChange(0);
        channel1.noteOn(i, velocitySlider.getValue());
        statusPane.setText("STATUS: Playing..");
        playB.setText("STOP");
    }

    /**
     * Stops matching note.
     *
     * @param i Pitch of the note
     */
    private void stopNote(int i) {
        final MidiChannel channel1 = synthesizer.getChannels()[0];
        channel1.noteOff(i, velocitySlider.getValue());
        statusPane.setText("STATUS: Stopped.");
        playB.setText("PLAY");
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!keyPressed) {
            int kc = e.getKeyCode();
            if (kc == 96 || kc == 49) {
                playNote(40);
                keyPressed = !keyPressed;
            }
            if (kc == 97 || kc == 50) {
                playNote(46);
                keyPressed = !keyPressed;
            }
            if (kc == 98 || kc == 51) {
                playNote(52);
                keyPressed = !keyPressed;
            }
            if (kc == 99 || kc == 52) {
                playNote(58);
                keyPressed = !keyPressed;
            }
            if (kc == 100 || kc == 53) {
                playNote(64);
                keyPressed = !keyPressed;
            }
            if (kc == 101 || kc == 54) {
                playNote(70);
                keyPressed = !keyPressed;
            }
            if (kc == 102 || kc == 55) {
                playNote(76);
                keyPressed = !keyPressed;
            }
            if (kc == 103 || kc == 56) {
                playNote(82);
                keyPressed = !keyPressed;
            }
            if (kc == 104 || kc == 57) {
                playNote(88);
                keyPressed = !keyPressed;
            }
            if (kc == 105 || kc == 48) {
                playNote(94);
                keyPressed = !keyPressed;
            }
        }
        System.out.println(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (keyPressed) {
            int kc = e.getKeyCode();
            if (kc == 96 || kc == 49) {
                stopNote(40);
                keyPressed = !keyPressed;
            }
            if (kc == 97 || kc == 50) {
                stopNote(46);
                keyPressed = !keyPressed;
            }
            if (kc == 98 || kc == 51) {
                stopNote(52);
                keyPressed = !keyPressed;
            }
            if (kc == 99 || kc == 52) {
                stopNote(58);
                keyPressed = !keyPressed;
            }
            if (kc == 100 || kc == 53) {
                stopNote(64);
                keyPressed = !keyPressed;
            }
            if (kc == 101 || kc == 54) {
                stopNote(70);
                keyPressed = !keyPressed;
            }
            if (kc == 102 || kc == 55) {
                stopNote(76);
                keyPressed = !keyPressed;
            }
            if (kc == 103 || kc == 56) {
                stopNote(82);
                keyPressed = !keyPressed;
            }
            if (kc == 104 || kc == 57) {
                stopNote(88);
                keyPressed = !keyPressed;
            }
            if (kc == 105 || kc == 48) {
                stopNote(94);
                keyPressed = !keyPressed;
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object object = e.getSource();

        if (object instanceof JMenuItem) {
            JMenuItem m = (JMenuItem) object;
            if (m.getText().equals("Close")) {
                System.exit(0);
            } else if (m.getText().equals("Save Soundbank")) {
                createFrame("Save Soundbank");

            } else if (m.getText().equals("New Soundbank")) {
                soundbank = new SF2Soundbank();
                instruments = new ArrayList<>(128);
                table.tableChanged(new TableModelEvent(dataModel));
            } else if (m.getText().equals("About")) {
                showAboutDialog();
            }
        } else if (object instanceof JButton) {
            JButton button = (JButton) object;
            if (button.equals(instSave)) {
                try {
                    insertinstrument(designInstrument(soundbank));
                    List gains = new ArrayList(width_sliders.length);
                    List widths = new ArrayList(width_sliders.length);
                    for (int i = 0; i < width_sliders.length; i++) {
                        gains.add(gain_sliders[i].getValue());
                        widths.add(width_sliders[i].getValue());
                    }
                    instrumentsGains.add(gains);
                    instrumentsWidths.add(widths);
                    sliderPanel.grabFocus();
                } catch (Exception ex) {
                    statusPane.setText(ex.toString());
                    System.out.println(ex);
                }
                statusPane.setText("STATUS: Instrument " + instName.getText() + " saved.");
            } else if (button.getText().equals("CLEAR")) {
                for (int i = 0; i < width_sliders.length; i++) {
                    width_sliders[i].setValue(0);
                    gain_sliders[i].setValue(0);
                }
                for (SFGeneratorSlider gen : generators) {
                    gen.setValue(gen.getDefaultValue());
                }
                statusPane.setText("STATU: Data cleared.");
            } else if (button.equals(instDelete)) {
                SF2Instrument temp = (SF2Instrument) instruments.get(row);
                soundbank.removeInstrument(temp);
                instruments.remove(row);
                table.tableChanged(new TableModelEvent(dataModel));
                statusPane.setText("STATUS: Instrument " + temp.getName() + " removed.");
            } else if (button.equals(playB)) {
                final MidiChannel channel1 = synthesizer.getChannels()[0];
                channel1.controlChange(7, 127);
                if (button.getText().equals("PLAY")) {
                    synthesizer.unloadAllInstruments(playSoundbank);
                    synthesizer.loadInstrument(designInstrument(playSoundbank));
                    channel1.programChange(0);
                    channel1.noteOn(50, 120);
                    statusPane.setText("STATUS: Playing..");
                    button.setText("STOP");
                } else {
                    channel1.noteOff(50, 120);
                    button.setText("PLAY");
                    statusPane.setText("");
                }
            } else if (button.getText().equals("Cancel")) {
                frame.dispose();
                frame = null;
            } else if (button.getText().equals("Save Soundbank")) {
                buildSoundbank(fileName.getText(), sbVendor.getText(), sbDescription.getText());
                statusPane.setText("STATUS: Soundbank saved as " + fileName.getText() + ".");
                frame.dispose();
                frame = null;
            }
        } else if (object instanceof JComboBox) {
            transferFocus();
            sliderPanel.requestFocusInWindow();
        } else if (object instanceof JTextField) {
            transferFocus();
            sliderPanel.requestFocusInWindow();
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SoundbankBuilder sBB = new SoundbankBuilder();

        JFrame f = new JFrame("Soundbank Builder");
        f.addKeyListener(sBB);

        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.getContentPane().add("Center", sBB);
        f.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int w = 1024;
        int h = 700;
        f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
        f.setSize(w, h);
        f.show();
        sBB.open();

    }

    /**
     * Method for creating new instrument. Uses data from amplitude, bandwidth
     * and generators sliders for building new instrument with suitable samples
     * and regions.
     *
     * @param sf2 Destination soundbank for new samples and regions.
     * @return Returns newly build instrument.
     */
    public SF2Instrument designInstrument(SF2Soundbank sf2) {
        int x = 8;
        int fftsize = 4096 * x;
        if (data == null || data.length != fftsize * 2) {
            data = new double[fftsize * 2];
        }

        Arrays.fill(data, 0);
        double[] data = this.data;
        double base = x * 15;
        //plnim data pomocou hodnot zo sliderov
        for (int i = 0; i < width_sliders.length; i++) {
            double harmonic = i + 1;
            try {
                harmonic = Double.parseDouble(harmonic_field[i].getText());
            } catch (NumberFormatException ex) {
                harmonic_field[i].setText("" + (i + 1));
            }
            if (gain_sliders[i].getValue() > 0) {
                double width = (width_sliders[i].getValue()) / 4;
                double db = -(100 - gain_sliders[i].getValue());
                double gain = Math.pow(10, db / 20.0);
                complexGaussianDist(data, base * harmonic, width, gain);
            }
        }

        SF2Layer layer1 = null;
        SF2Layer layer2 = null;

        //dva for cykly pre data_audio 1&2
        for (int i = 0; i < 2; i++) {
            double[] data_audio = i == 0 ? this.data_audio : this.data_audio2;
            if (data_audio == null || data_audio.length != fftsize * 2) {
                data_audio = new double[fftsize * 2];
                if (i == 0) {
                    this.data_audio = data_audio;
                } else {
                    this.data_audio2 = data_audio;
                }
            }
            //kopirujem data do data_audio
            System.arraycopy(data, 0, data_audio, 0, data_audio.length);
            SF2Sample sample = newSF2Sample(sf2, instName.getText() + " Sample " + i,
                    data_audio, base, 200);
            SF2Layer layer = newLayer(sf2, instName.getText() + " Layer " + i, sample);
            SF2Region region = layer.getRegions().get(0);
            region.putInteger(SF2Region.GENERATOR_SAMPLEMODES, 1); // set loop
            if (stereo_mode) {
                if (i == 0) {
                    region.putInteger(SF2Region.GENERATOR_PAN, -500);
                } else {
                    region.putInteger(SF2Region.GENERATOR_PAN, 500);
                }
            }
            for (SFGeneratorSlider gen : generators) {
                gen.process(region);
            }

            if (i == 0) {
                layer1 = layer;
            } else {
                layer2 = layer;
            }

            if (!stereo_mode) {
                break;
            }
        }

        /*
         * Create SoundFont2 instrument.
         */
        SF2Instrument ins = new SF2Instrument();
        ins.setPatch(new Patch(0, 0));
        ins.setName(instName.getText());
        sf2.addInstrument(ins);

        /*
         * Create region for instrument.
         */
        if (!stereo_mode) {
            SF2InstrumentRegion insregion = new SF2InstrumentRegion();
            insregion.setLayer(layer1);
            ins.getRegions().add(insregion);
        } else {
            SF2InstrumentRegion insregion = new SF2InstrumentRegion();
            insregion.setLayer(layer1);
            ins.getRegions().add(insregion);
            insregion = new SF2InstrumentRegion();
            insregion.setLayer(layer2);
            ins.getRegions().add(insregion);
        }
        return ins;

    }

    /**
     * Saves new instrument into the given soundbank as well as the control
     * array.
     *
     * @param instrument Instrument to be saved.
     */
    public void insertinstrument(SF2Instrument instrument) {
//        soundbank.addInstrument(instrument);
        instruments.add(instrument);
        table.tableChanged(new TableModelEvent(dataModel));
    }

    /**
     * Loads amplitude and bandwidth data for selected instrument.
     */
    private void loadInstrumentData() {
        List gains = instrumentsGains.get(row);
        List widths = instrumentsWidths.get(row);
        for (int i = 0; i < width_sliders.length; i++) {
            gain_sliders[i].setValue((Integer) gains.get(i));
            width_sliders[i].setValue((Integer) widths.get(i));
        }
    }

    /**
     * Builds soundbank for output and exports it into given file.
     *
     * @param name Name of the soundbank and the output file.
     * @param vendor Set vendor signature
     * @param desc set description of the soundbank
     */
    public void buildSoundbank(String name, String vendor, String desc) {
        soundbank.setName(name);
        soundbank.setVendor(vendor);
        soundbank.setDescription(desc);
        try {
            soundbank.save(name + ".sf2");
        } catch (IOException ex) {
            statusPane.setText("ERROR: " + ex);
        }
    }

    /**
     * Builds SF2 Sample out of given data and saves it into the fiven SF2
     * soundbank.
     *
     * @param sf2 Destination soundbank
     * @param name Name of the sample
     * @param data Data for sample
     * @param base Value for base fequency
     * @param fadeuptime Fade up value
     * @return
     */
    public SF2Sample newSF2Sample(SF2Soundbank sf2, String name,
            double[] data, double base, int fadeuptime) {

        int fftsize = data.length / 2;
        AudioFormat format = new AudioFormat(44100, 16, 1, true, false);
        double basefreq = (base / fftsize) * format.getSampleRate() * 0.5;

        randomPhase(data);
        ifft(data);
        data = realPart(data);
        normalize(data, 0.9);
        float[] fdata = toFloat(data);
        fdata = loopExtend(fdata, fdata.length + 512);
        fadeUp(fdata, fadeuptime);
        byte[] bdata = toBytes(fdata, format);

        /*
         * Create SoundFont2 sample.
         */
        SF2Sample sample = new SF2Sample(sf2);
        sample.setName(name);
        sample.setData(bdata);
        sample.setStartLoop(256);
        sample.setEndLoop(fftsize + 256);
        sample.setSampleRate((long) format.getSampleRate());
        double orgnote = (69 + 12)
                + (12 * Math.log(basefreq / 440.0) / Math.log(2));
        sample.setOriginalPitch((int) orgnote);
        sample.setPitchCorrection((byte) (-(orgnote - (int) orgnote) * 100.0));
        sf2.addResource(sample);

        return sample;
    }

    /**
     * Creates new SF2Layer object.
     *
     * @param sf2 Soundbank for new layer
     * @param name Name of the layer
     * @param sample Sample data to be stored in the layer
     * @return
     */
    public SF2Layer newLayer(SF2Soundbank sf2, String name, SF2Sample sample) {
        SF2LayerRegion region = new SF2LayerRegion();
        region.setSample(sample);

        SF2Layer layer = new SF2Layer(sf2);
        layer.setName(name);
        layer.getRegions().add(region);
        sf2.addResource(layer);

        return layer;
    }

    public void randomPhase(double[] data) {
        for (int i = 0; i < data.length; i += 2) {
            double phase = Math.random() * 2 * Math.PI;
            double d = data[i];
            data[i] = Math.sin(phase) * d;
            data[i + 1] = Math.cos(phase) * d;
        }
    }

    public double[] realPart(double[] in) {
        double[] out = new double[in.length / 2];
        for (int i = 0; i < out.length; i++) {
            out[i] = in[i * 2];
        }
        return out;
    }

    public void fadeUp(float[] data, int samples) {
        double dsamples = samples;
        for (int i = 0; i < samples; i++) {
            data[i] *= i / dsamples;
        }
    }

    public byte[] toBytes(float[] in, AudioFormat format) {
        byte[] out = new byte[in.length * format.getFrameSize()];
        return AudioFloatConverter.getConverter(format).toByteArray(in, out);
    }

    public void normalize(double[] data, double target) {
        double maxvalue = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] > maxvalue) {
                maxvalue = data[i];
            }
            if (-data[i] > maxvalue) {
                maxvalue = -data[i];
            }
        }
        if (maxvalue == 0) {
            return;
        }
        double gain = target / maxvalue;
        for (int i = 0; i < data.length; i++) {
            data[i] *= gain;
        }
    }

    public float[] toFloat(double[] in) {
        float[] out = new float[in.length];
        for (int i = 0; i < out.length; i++) {
            out[i] = (float) in[i];
        }
        return out;
    }

    public float[] loopExtend(float[] data, int newsize) {
        float[] outdata = new float[newsize];
        int p_len = data.length;
        int p_ps = 0;
        for (int i = 0; i < outdata.length; i++) {
            outdata[i] = data[p_ps];
            p_ps++;
            if (p_ps == p_len) {
                p_ps = 0;
            }
        }
        return outdata;
    }

    public void ifft(double[] data) {
        FFT ifft_obj = null;
        int ifft_obj_len = 0;

        if (ifft_obj == null || ifft_obj_len != data.length / 2) {
            ifft_obj_len = data.length / 2;
            ifft_obj = new FFT(ifft_obj_len, 1);

        }
        ifft_obj.transform(data);
    }

    /**
     * Builds complex Gaussian Distribution Function. BUilds Gaussian function
     * using given data and stores it into the double[] data stream.
     *
     * @param data Data stream for new function
     * @param m
     * @param width
     * @param gain
     */
    public void complexGaussianDist(double[] data, double m, double width, double gain) {
        if (width < 0.5) {
            int im = (int) m;
            data[im * 2] = gain;
            return;
        }
        for (int x = 0; x < data.length / 4; x++) {
            data[x * 2] += gain * Math.exp((-1.0 / 2.0) * Math.pow((x - m) / width, 2.0));
        }
    }

    /**
     * Object for GeneratorSlider.
     */
    private class SFGeneratorSlider extends JPanel {

        private static final long serialVersionUID = 1L;
        private int genId;
        private JSlider slider;

        private short getSFDefaultValue(int generator) {
            if (generator == 8) {
                return (short) 13500;
            }
            if (generator == 21) {
                return (short) -12000;
            }
            if (generator == 23) {
                return (short) -12000;
            }
            if (generator == 25) {
                return (short) -12000;
            }
            if (generator == 26) {
                return (short) -12000;
            }
            if (generator == 27) {
                return (short) -12000;
            }
            if (generator == 28) {
                return (short) -12000;
            }
            if (generator == 30) {
                return (short) -12000;
            }
            if (generator == 33) {
                return (short) -12000;
            }
            if (generator == 34) {
                return (short) -12000;
            }
            if (generator == 35) {
                return (short) -12000;
            }
            if (generator == 36) {
                return (short) -12000;
            }
            if (generator == 38) {
                return (short) -12000;
            }
            if (generator == 43) {
                return (short) 0x7F00;
            }
            if (generator == 44) {
                return (short) 0x7F00;
            }
            if (generator == 46) {
                return (short) -1;
            }
            if (generator == 47) {
                return (short) -1;
            }
            if (generator == 56) {
                return (short) 100;
            }
            if (generator == 58) {
                return (short) -1;
            }
            return 0;
        }

        public int getDefaultValue() {
            return getSFDefaultValue(genId);
        }

        private short getSFMinValue(int generator) {
            switch (generator) {
                case 0:
                    return 0;
                case 1:
                    return -32768;
                case 2:
                    return -32768;
                case 3:
                    return -32768;
                case 4:
                    return 0;
                case 5:
                    return -12000;
                case 6:
                    return -12000;
                case 7:
                    return -12000;
                case 8:
                    return 1500;
                case 9:
                    return 0;
                case 10:
                    return -12000;
                case 11:
                    return -12000;
                case 12:
                    return -32768;
                case 13:
                    return -960;
                case 15:
                    return 0;
                case 16:
                    return 0;
                case 17:
                    return -500;
                case 21:
                    return -16000;
                case 22:
                    return -12000;
                case 23:
                    return -16000;
                case 24:
                    return -12000;
                case 25:
                    return -12000;
                case 26:
                    return -12000;
                case 27:
                    return -12000;
                case 28:
                    return -12000;
                case 29:
                    return 0;
                case 30:
                    return -12000;
                case 31:
                    return -1200;
                case 32:
                    return -1200;
                case 33:
                    return -12000;
                case 34:
                    return -12000;
                case 35:
                    return -12000;
                case 36:
                    return -12000;
                case 37:
                    return 0;
                case 38:
                    return -12000;
                case 39:
                    return -1200;
                case 40:
                    return -1200;
                case 43:
                    return 0;
                case 44:
                    return 0;
                case 45:
                    return -32768;
                case 46:
                    return 0;
                case 47:
                    return 0;
                case 48:
                    return 0;
                case 50:
                    return -32768;
                case 51:
                    return -120;
                case 52:
                    return -99;
                case 54:
                    return -32768;
                case 56:
                    return 0;
                case 57:
                    return 1;
                case 58:
                    return 0;
                default:
                    return Short.MIN_VALUE;
            }
        }

        private String getSFName(int generator) {
            if ((generator == 21) || (generator == 23) || (generator == 25) || (generator == 33)) {
                return "Delay";
            }
            if ((generator == 26) || (generator == 34)) {
                return "Attack";
            }
            if ((generator == 27) || (generator == 35)) {
                return "Hold";
            }
            if ((generator == 28) || (generator == 36)) {
                return "Decay";
            }
            if ((generator == 29) || (generator == 37)) {
                return "Sustain";
            }
            if ((generator == 30) || (generator == 38)) {
                return "Release";
            }
            if ((generator == 10) || (generator == 11)) {
                return "To Filter Cutoff";
            }
            if ((generator == 5) || (generator == 6) || (generator == 7)) {
                return "To Pitch";
            }
            if ((generator == 22) || (generator == 24)) {
                return "Frequency";
            }
            if (generator == 48) {
                return "Attenuation";
            }
            return "ERROR";
        }

        private short getSFMaxValue(int generator) {
            switch (generator) {
                case 0:
                    return 32767;
                case 1:
                    return 0;
                case 2:
                    return 32767;
                case 3:
                    return 32767;
                case 4:
                    return 32767;
                case 5:
                    return 12000;
                case 6:
                    return 12000;
                case 7:
                    return 12000;
                case 8:
                    return 13500;
                case 9:
                    return 960;
                case 10:
                    return 12000;
                case 11:
                    return 12000;
                case 12:
                    return 0;
                case 13:
                    return 960;
                case 15:
                    return 1000;
                case 16:
                    return 1000;
                case 17:
                    return 500;
                case 21:
                    return 5000;
                case 22:
                    return 4500;
                case 23:
                    return 5000;
                case 24:
                    return 4500;
                case 25:
                    return 5000;
                case 26:
                    return 8000;
                case 27:
                    return 5000;
                case 28:
                    return 8000;
                case 29:
                    return 1000;
                case 30:
                    return 8000;
                case 31:
                    return 1200;
                case 32:
                    return 1200;
                case 33:
                    return 5000;
                case 34:
                    return 8000;
                case 35:
                    return 5000;
                case 36:
                    return 8000;
                case 37:
                    return 1440;
                case 38:
                    return 8000;
                case 39:
                    return 1200;
                case 40:
                    return 1200;
                case 43:
                    return 127;
                case 44:
                    return 127;
                case 45:
                    return 32767;
                case 46:
                    return 127;
                case 47:
                    return 127;
                case 48:
                    return 1440;
                case 50:
                    return 32767;
                case 51:
                    return 120;
                case 52:
                    return 99;
                case 54:
                    return 32767;
                case 56:
                    return 1200;
                case 57:
                    return 127;
                case 58:
                    return 127;
                default:
                    return Short.MAX_VALUE;
            }
        }

        public SFGeneratorSlider(final int genId) {
            this.genId = genId;

            setOpaque(true);

            TitledBorder tb = new TitledBorder(new EtchedBorder());
            tb.setTitle(getSFName(genId));

            slider = new JSlider();
            slider.setOpaque(true);
            slider.setMinimum(getSFMinValue(genId));
            slider.setMaximum(getSFMaxValue(genId));
            slider.setValue(getSFDefaultValue(genId));
            slider.setMaximumSize(new Dimension(150, 18));
            slider.setPreferredSize(new Dimension(150, 18));
            slider.setFocusable(false);
            slider.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    statusPane.setText("STATUS: Generator <" + getSFName(genId) + "> changed to :" + Integer.toString(slider.getValue()));
                }
            });
            add(slider);

            generators.add(this);
        }

        public int getValue() {
            return slider.getValue();
        }

        public void setValue(int i) {
            if (slider.getValue() != i) {
                slider.setValue(i);
            }
        }

        public void process(SF2Region region) {
            if (slider.getValue() != getSFDefaultValue(genId)) {
                region.putInteger(genId, slider.getValue());
            }
        }
    }

    /**
     * New definition of slider panel. Allows the use of the scale backround.
     */
    public class myPanel extends JPanel {

        private Image image = null;

        public myPanel(String filename) {
            this.image = new ImageIcon(filename).getImage();
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, image.getWidth(null), image.getHeight(null), null);
        }

        @Override
        public void grabFocus() {
            requestFocus();
        }
    }

    /**
     * Creates frame for exporting files.
     *
     * @param titleName Name of the frame.
     */
    public void createFrame(String titleName) {
        int w = 500;
        int h = 300;
//        JPanel panel = new JPanel(new BorderLayout());
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JPanel p1 = new JPanel();
        p1.add(new JLabel("File or Dir :   "));
        String sep = String.valueOf(System.getProperty("file.separator").toCharArray()[0]);
        String text = null;
        try {
            text = System.getProperty("user.dir") + sep;
        } catch (SecurityException ex) {
            statusPane.setText(ex.toString());
            return;
        }
        fileName = new JTextField(text);
        fileName.setPreferredSize(new Dimension(w - 100, 30));
        fileName.addActionListener(this);
        p1.add(fileName);
        panel.add(p1);

        JPanel p2 = new JPanel();
        p2.add(new JLabel("Vendor :       "));
        sbVendor = new JTextField("Generated");
        sbVendor.setPreferredSize(new Dimension(w - 100, 30));
        p2.add(sbVendor);
        panel.add(p2);

        JPanel p3 = new JPanel();
        p3.add(new JLabel("Description : "));
        sbDescription = new JTextArea("");
        sbDescription.setPreferredSize(new Dimension(w - 100, 90));
        p3.add(sbDescription);
        panel.add(p3);

        JPanel p4 = new JPanel();
        JButton applyB = createButton("Save Soundbank", getBackground());
        p4.add(applyB);
        p4.add(createButton("Cancel", getBackground()));
        panel.add("South", p4);
        frame = new JFrame(titleName);
        frame.getContentPane().add("Center", panel);
        frame.pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(d.width / 2 - w / 2, d.height / 2 - h / 2);
        frame.setSize(w, h);
        frame.setVisible(true);
    }

    /**
     * Dialog in the [About] Frame.
     */
    private static void showAboutDialog() {
        final String msg =
                "Soundbank Builder Application serves for building your very own SF2 Sounbank.\n\n"
                + "Designing an Instrument:\n"
                + "Vertical sliders of the top part change amplitude of the signal.\n"
                + "Horizontal sliders of the top part change the bandwidth of correspondent samples.\n"
                + "Sliders in the bottom part serve as aditional generation settings. \n\n"
                + "Test play:\n"
                + "Use the number keys or numpad to try newly designed instrument or click the PLAY\n"
                + "button for continuous tone if you are brave enough. \n\n"
                + "Saving the Instrument:\n"
                + "[Save] your designed instrument and start working on another. All instruments are stored\n"
                + "in the table on the right. You can [Delete] them or load the main data with mouse click the row.\n\n"
                + "Exporting Soundbanks: \n"
                + "Choose [File] -> [Save Soundbank] for exporting instruments, then enter the Name, Vendor and Description\n"
                + "for new soundbank. All visible instruments will be exported as one big SF2 Soundbank file.\n";
        new Thread(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(null, msg, "Applet Info", JOptionPane.INFORMATION_MESSAGE);
            }
        }).start();
    }
}
