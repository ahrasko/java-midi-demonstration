/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javamididemo;

import java.io.File;
import java.io.IOException;

import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;

/**
 *
 * @author Andrej Hraško
 */
public class SFImportTest {

    private static final boolean DEBUG = true;

    public static void main(String[] args) 
            throws MidiUnavailableException, InvalidMidiDataException, IOException {
        int nNoteNumber = 66;	// MIDI key number
        int nVelocity = 100;	// MIDI note on velocity

        /*
         *	Time between note on and note off event in
         *	milliseconds. Note that on most systems, the
         *	best resolution you can expect are 10 ms.
         */
        int nDuration = 2000;

        Soundbank soundbank = null;
        File file = new File("RolandChurchBells.sf2");
        soundbank = MidiSystem.getSoundbank(file);
        
        System.out.println("DESCRIPTION: " +soundbank.getName());
        if (DEBUG) {
            out("Soundbank: " + soundbank);
        }


        /*
         *	We need a synthesizer to play the note on.
         *	Here, we simply request the default
         *	synthesizer.
         */
        Synthesizer synth = null;
        synth = MidiSystem.getSynthesizer();
        if (DEBUG) {
            out("Synthesizer: " + synth);
        }

        /*
         *	Of course, we have to open the synthesizer to
         *	produce any sound for us.
         */
        synth.open();
        if (DEBUG) {
            out("Defaut soundbank: " + synth.getDefaultSoundbank());
        }

        if (soundbank != null) {
            out("soundbank supported: " + synth.isSoundbankSupported(soundbank));
            boolean bInstrumentsLoaded = synth.loadAllInstruments(soundbank);
            
            Instrument instruments[] = synth.getLoadedInstruments();
            for(int i = 0; i < instruments.length; i++){
                System.out.println(instruments[i].getName());
            }
            
            if (DEBUG) {
                out("Instruments loaded: " + bInstrumentsLoaded);
            }
        }
        /*
         *	Turn the note on on MIDI channel 1.
         *	(Index zero means MIDI channel 1)
         */
        MidiChannel[] channels = synth.getChannels();
        channels[0].noteOn(nNoteNumber, nVelocity);

        /*
         *	Wait for the specified amount of time
         *	(the duration of the note).
         */
        try {
            Thread.sleep(nDuration);
        } catch (InterruptedException e) {
        }

        /*
         *	Turn the note off.
         */
        channels[0].noteOff(nNoteNumber);
    }

    private static void printUsageAndExit() {
        out("LoadSoundbank: usage:");
        out("java LoadSoundbank [<soundbankfilename>]");
        System.exit(1);
    }

    private static void out(String strMessage) {
        System.out.println(strMessage);
    }
}
