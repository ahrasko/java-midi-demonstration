# README #

For running this app you need to have JRE version 1.2.0 or better. 
You can run .exe or .jar files(in dist dir). 
Project was written in NetBeans IDE version 7.2. 

This application is created as a part of masters thesis 'Sound synthesis in Java and project Gervill'.

Description:
**Groove App** serves for creating beats by checking cells int the main grid.

Building the beat: 
Just click the cells in the grid and press [Start]. [Loop] button will enable continuous playing. You      can still edit the grid during the loop. If you are not satisfied with your first try, feel free to [Clear] the grid and build new, better beat. Uncheck [Fixed ticks] if you want to build track shorter than 20 ticks.
Instruments:
You can [Add] new Instruments into the grid by choosing one from the drop down list and setting the note and velocity of it. You can also [Edit] existing instrument by selecting it in the grid and changing its settings. [Clear Instruments] will Delete every Instrument in the grid.
Save & Export: 
[Save] your track into .sql file for later work or [Export] it as a MIDI file.

**Song Builder** builds songs from sample tracks created in the Groove Application.

Building songs:
To build a song first [Add] tracks into the grid. After that, you can select cells in the rows to choose when will the coresponding track play. Mind the info about the duration of tracks so the won't play one trough another.
Saving: 
To save the song simply choose [Export ...] option in the [File] menu.Come with catchy name, hit [Export] and done. Your song is ready to play.

**Key Synthesizer** serves for recording your own tracks using keyboard keys as synth keys.

Playing: 
Just choose instrument you like from the combobox, row you want to put it in, set the note range and press [Edit Instrument]. The key line is ready to rock.
Recording: To record your playing, just press the [Record] button and hit the keyboard. Recording will be stopped by the same button. If you want to add another sound to your track, check the [Play & Record] option and the track won't be overwritten but it will record new sounds into the old track while playing it.
Exporting: After choosing to [Export] to MIDI or WAV just write the desired name and the song will be rendered and saved in the given directory.

* Author: Andrej Hraško.
* Version 1.0
