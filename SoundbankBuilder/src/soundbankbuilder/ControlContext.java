
package soundbankbuilder;

/**
 *
 * @author Andrej Hraško
 */
interface ControlContext {

    public void open();

    public void close();
}
